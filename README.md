# Test Data Loader

## Test data load for the first personal notification query
- Lambda Function : ACT Notification Data Loader
- Add CloudWatch Log Trigger to the Data Loader Lambda Function
- Trigger is only set for SIT Env

## Test Result Portal
[QA Dashboard](http://qaovmc1e:5601/app/kibana#/dashboards?_g=())


## Step by step procedure to find Android UDID:

1. Go to settings for your Mobile device -> About Phone -> Build Number -> Tap 7 times to enable Developer options.

2. Go to Developer options , under Debugging -> Make sure to enable USB debugging ON.

3. Now connect your mobile device(using usb cable) to your PC.

4. Open Command Prompt and type below command
   -> adb devices

[Web Reference](http://qahumor.blogspot.com/2015/06/appium-how-to-get-udid-of-android-device.html)