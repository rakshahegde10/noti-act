<!--
  Copyright (c) Hospital Authority 2018.
  All Rights Reserved.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>hk.org.ha.qa.service.tch.act</groupId>
	<artifactId>act-common-shared-lib</artifactId>
	<version>2.4.0-SNAPSHOT</version>
	<name>${project.artifactId}</name>
	<description>Autonomous Continuous Testing Common Shared Library</description>
	<distributionManagement>
	    <snapshotRepository>
	        <id>snapshots</id>
	        <name>idfvmc6a-snapshots</name>
	        <url>http://idfvmc6a:55742/artifactory/int_lib_ta_snapshot</url>
	    </snapshotRepository>
        <repository>
	        <id>central</id>
	        <name>idfvmc6a-releases</name>
	        <url>http://idfvmc6a:55742/artifactory/lib_ta</url>
    	</repository>
	</distributionManagement>

	<!-- properties --> 
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<build.number>BUILD_NUMBER</build.number>
		<!-- deployment settings -->
		<build.filename>${project.artifactId}-${project.version}</build.filename>
		<cucumber.options></cucumber.options>
		<application.name></application.name>
		<test.institution></test.institution>
		<test.env></test.env>
		<video.capture></video.capture>
		<screen.capture></screen.capture>
		<report.generation></report.generation>
		<queryDB></queryDB>
		<ie.require.window.focus></ie.require.window.focus>
		<selenium.hub.address>HUB-ADDRESS</selenium.hub.address>
		<!-- plugin version -->
		<org.apache.maven.plugins.maven-compiler-plugin.version>2.3.2</org.apache.maven.plugins.maven-compiler-plugin.version>
		<org.apache.maven.plugins.maven-jar-plugin.version>2.4</org.apache.maven.plugins.maven-jar-plugin.version>
		<org.apache.maven.plugins.maven-install-plugin.version>2.5.2</org.apache.maven.plugins.maven-install-plugin.version>
		<org.apache.maven.plugins.maven-deploy-plugin.version>2.7</org.apache.maven.plugins.maven-deploy-plugin.version>
		<!-- dependencies version -->
	</properties>
	
	<!-- build -->
	<build>
		<defaultGoal>package</defaultGoal>		
		<finalName>${build.filename}</finalName>
		
		<plugins>
		
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${org.apache.maven.plugins.maven-compiler-plugin.version}</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<systemPropertyVariables>
						<org.uncommons.reportng.frames>false</org.uncommons.reportng.frames>
					</systemPropertyVariables>
				</configuration>
			</plugin>
			
			<plugin>
	            <groupId>org.apache.maven.plugins</groupId>
	            <artifactId>maven-surefire-plugin</artifactId>
	            <version>2.9</version><!--version>2.17</version-->
	            <configuration>
	            	<redirectTestOutputToFile>true</redirectTestOutputToFile>
					<reportFormat>plain</reportFormat>
	                <!--suiteXmlFiles-->
	                	<!--suiteXmlFile>TestSuite/TestNG.xml</suiteXmlFile-->
	                <!--/suiteXmlFiles-->
	                <!--skipTests>false</skipTests-->
	            </configuration>
	        </plugin>
		
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>${org.apache.maven.plugins.maven-jar-plugin.version}</version>
				<inherited>true</inherited>
				<configuration>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
						</manifest>
					</archive>
				</configuration>
			</plugin>
		
            <!--  install jar to local repository -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>  
                <artifactId>maven-install-plugin</artifactId>
                <version>${org.apache.maven.plugins.maven-install-plugin.version}</version>
                <executions>
                    <execution>
                        <phase>install</phase>
                        <goals>
                            <goal>install-file</goal>
                        </goals>
                        <configuration>
                            <packaging>jar</packaging>
                            <artifactId>${project.artifactId}</artifactId>
                            <groupId>${project.groupId}</groupId>
                            <version>${project.version}</version>
                            <file>
                                ${project.build.directory}/${project.artifactId}-${project.version}.jar
                            </file>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!--  deploy jar to remote repository -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>${org.apache.maven.plugins.maven-deploy-plugin.version}</version>
                <executions>
                    <execution>
                        <phase>deploy</phase>
                        <goals>
                            <goal>deploy-file</goal>
                        </goals>
                        <configuration>
                            <packaging>jar</packaging>
                            <generatePom>true</generatePom>
                            <url>${project.distributionManagement.snapshotRepository.url}</url>
                            <artifactId>${project.artifactId}</artifactId>
                            <groupId>${project.groupId}</groupId>
                            <version>${project.version}</version>
                            <file>${project.build.directory}/${project.artifactId}-${project.version}.jar</file>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

		</plugins>
	</build>
	
	<dependencies>
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-java</artifactId>
			<version>4.2.0</version>
			<scope>compile</scope>
		</dependency>
  		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-server</artifactId>
			<version>3.141.59</version>
			<!-- <version>3.12.0</version> -->
			<scope>compile</scope>
		</dependency>
 		
		<dependency>
			<groupId>io.cucumber</groupId>
			<artifactId>cucumber-testng</artifactId>
			<version>4.2.0</version>
			<scope>compile</scope>
			<exclusions>
				<exclusion>
					<groupId>junit</groupId>
					<artifactId>junit</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- dependency>
			<groupId>org.testng</groupId>
			<artifactId>testng</artifactId>
			<version>6.10</version>
			<scope>test</scope>
		</dependency> -->

		
		<dependency>
    		<groupId>io.appium</groupId>
    		<artifactId>java-client</artifactId>
    		<version>7.3.0</version>
    		<!-- <version>6.1.0</version> -->
    		<scope>compile</scope>
		</dependency>			
		<dependency>
    		<groupId>com.google.guava</groupId>
    		<artifactId>guava</artifactId>
    		<version>22.0</version>
    		<scope>compile</scope>
		</dependency>
		
		<dependency> 
			<groupId>org.apache.poi</groupId> 
			<artifactId>poi</artifactId> 
			<version>3.17</version> 
    		<scope>compile</scope>
		</dependency>
 
		<dependency> 
			<groupId>org.apache.poi</groupId> 
			<artifactId>poi-ooxml</artifactId> 
			<version>3.17</version> 
    		<scope>compile</scope>
		</dependency>
		<dependency>
		    <groupId>net.sourceforge.tess4j</groupId>
		    <artifactId>tess4j</artifactId>
		    <version>3.4.2</version>
		</dependency>
		<dependency>
		    <groupId>org.apache.derby</groupId>
		    <artifactId>derby</artifactId>
		    <version>10.14.2.0</version>
		    <scope>compile</scope>
		</dependency>
		
		<!--  For DB Bridge -->
		<dependency>
		    <groupId>com.caucho</groupId>
		    <artifactId>hessian</artifactId>
		    <version>3.1.3</version>
		</dependency>
		<!-- 
		<dependency>
		    <groupId>wlfullclient</groupId>
		    <artifactId>wlfullclient</artifactId>
		    <version>10.3</version>
		</dependency> -->
		<!-- <dependency>
			<groupId>ha.tool.taf.service.common</groupId>
			<artifactId>taf-common-dataservice-bridge-client-ejb</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>ha.tool.taf.service.common</groupId>
			<artifactId>taf-common-dataservice-client-ejb</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency> -->
		<!-- <dependency>
			<groupId>blt-core</groupId>
			<artifactId>blt-core</artifactId>
			<version>1.13.0</version>
			<scope>system</scope>
			<systemPath>${basedir}\lib\blt-core-1.13.0.jar</systemPath>
			<systemPath>D:\eclipse_neon\eclipse_neon\workspace\taf-common-shared-lib\commonExtLib\blt-core-1.13.0.jar</systemPath>
		</dependency> -->
		
		<!-- <dependency>
			<groupId>com.sikulix</groupId>
			<artifactId>sikulix</artifactId>
			<version>1.1.3-SNAPSHOT</version>
		</dependency> -->
		
		<dependency>
			<groupId>ha.tool.act.service.common</groupId>
			<artifactId>selenium-sikuli-client</artifactId>
			<version>2.1.0-SNAPSHOT</version>
		</dependency>
		
	</dependencies>
	
</project>
