package stepDefinitions;

import java.util.List;
import java.util.Map;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.TouchAction;

public class HAGo extends HaGoCommonController{
	private String skip;
	private String jailbreakConfirm;
	private String virusOK;
	//hago.newVersionCancel = id: android:id/button1
	private String newVersionCancel;
	private String virusNotification;
	private String strFreqUsedApp;
	private String login;
	private String username;
	private String password;
	private String login2;
	
	private String useTouchIDNo;
	private String touchIDEnableLaterMsg;
	private String logout;
	private String logoutConfirmYes;
			
	private String profile;
	private String emailEdit;
	private String emailEditPassword;
	private String emailEditConfirm;
	private String emailEditNewEmail;
	private String emailEditSave;
	private String emailEditConfirmLater;
	private String emailEditChangeNotEffective;
	private String profileBackSymbol;
	
	
	private String bookha;
	private String bookhaAfterLogin;
	private String btnBookhaBackSymbol;
	
	// App - My Appointments 
    private String 	appMyappt;
	private String 	tabFuture;
	private String 	tabPast;
	private String 	btnFutureShowCancelRescheduleRecord;
	private String 	btnPastShowCancelRescheduleRecord;
	private String  btnMyapptBackSymbol;

	
	// Button
	// switch langue: Chinese to English (C2E) 
	private String moreC;
	private String moreLanguageOptionC;
	private String moreLanguageOptionSelectEnglishC;
	private String homeC;
	// switch langue: English to Chinese (E2C) 
	private String moreE;
	private String moreLanguageOptionE;
	private String moreLanguageOptionSelectChineseE;
	private String homeE;			
	
	private String otp1;
	private String otp2;
	private String otp3;
	private String otp4;
	private String otp5;
	private String otp6;
	private String otpSubmit;
			
	private String payhaIcon;
	private String payhaView;
	private String payhaTotal;
	
	public HAGo() throws Exception {
		skip = getElDef("hago.skip");
		jailbreakConfirm = getElDef("hago.jailbreakConfirm");
		virusOK = getElDef("hago.virusOK");
		newVersionCancel = getElDef("hago.newVersionCancel");
		virusNotification = getElDef("hago.virusNotification");
		strFreqUsedApp = getElDef("hago.strFreqUsedApp");
		System.out.println("virusOK:"+virusOK);
		//switch language: C2E
		moreC=	getElDef("hago.moreC");	
		moreLanguageOptionC=	getElDef("hago.moreLanguageOptionC");
		moreLanguageOptionSelectEnglishC=	getElDef("hago.moreLanguageOptionSelectEnglishC");
		homeE=	getElDef("hago.homeE");
		
		//switch language: E2C
		moreE=	getElDef("hago.moreE");	
		moreLanguageOptionE=	getElDef("hago.moreLanguageOptionE");
		moreLanguageOptionSelectChineseE=	getElDef("hago.moreLanguageOptionSelectChineseE");
		homeC=	getElDef("hago.homeC");
		
		
		otp1 = 	getElDef("hago.otp1");
		otp2 = 	getElDef("hago.otp2");
		otp3 = 	getElDef("hago.otp3");
		otp4 = 	getElDef("hago.otp4");
		otp5 = 	getElDef("hago.otp5");
		otp6 = 	getElDef("hago.otp6");
		otpSubmit = 	getElDef("hago.otp.submit");
		
	}
	public void elementDefinition(String strStartsWith) throws Exception {
		
		login = getElDef(strStartsWith+".login");
		username = getElDef(strStartsWith+".username");
		password = getElDef(strStartsWith+".password");
		login2 = getElDef(strStartsWith+".login2");


		useTouchIDNo = getElDef(strStartsWith+".useTouchIDNo");
		touchIDEnableLaterMsg = getElDef(strStartsWith+".touchIDEnableLaterMsg");
		logout = getElDef(strStartsWith+".logout");
		logoutConfirmYes = getElDef(strStartsWith+".logoutConfirmYes");
		
		profile = getElDef(strStartsWith+".profile");
		emailEdit = getElDef(strStartsWith+".emailEdit");
		emailEditPassword = getElDef(strStartsWith+".emailEditPassword");
		emailEditConfirm = getElDef(strStartsWith+".emailEditConfirm");
		emailEditNewEmail = getElDef(strStartsWith+".emailEditNewEmail");
		emailEditSave = getElDef(strStartsWith+".emailEditSave");
		emailEditConfirmLater = getElDef(strStartsWith+".emailEditConfirmLater");
		emailEditChangeNotEffective = getElDef(strStartsWith+".emailEditChangeNotEffective");
		profileBackSymbol = getElDef(strStartsWith+".profileBackSymbol");
		appMyappt = getElDef(strStartsWith+".appMyappt");
		bookha = getElDef("hago.bookha");
		bookhaAfterLogin = getElDef("hago.bookhaAfterLogin");
		// App - My appointments		
		tabFuture = getElDef(strStartsWith+".myapptTabFuture");
		tabPast = getElDef(strStartsWith+".myapptTabPast");
		btnFutureShowCancelRescheduleRecord = getElDef(strStartsWith+".myapptBtnFutureShowCancelRescheduleRecord");
		btnPastShowCancelRescheduleRecord = getElDef(strStartsWith+".myapptBtnPastShowCancelRescheduleRecord");
		btnMyapptBackSymbol = getElDef(strStartsWith+".myapptBtnMyapptBackSymbol");
		
		payhaIcon = getElDef(strStartsWith + ".payha.icon");
		payhaView = getElDef(strStartsWith + ".payha.view");
		payhaTotal = getElDef(strStartsWith + ".payha.total");
	}
	@Given("open HAGo App$")
	public void openHAGoApp() throws Exception {
		
		//For ACT 2.4.0
		//mobileController.speedupDriver(false);
		
		if(elementExists(skip))
		{
			tap(skip);
    	
		}		
//		if(elementExists(jailbreakConfirm))
//		{
//			tap(jailbreakConfirm);
//    	
//		}	
		/*if(elementExists(virusOK))
		{
			tap(virusOK);
    	
		}	*/
		if(elementExists(newVersionCancel))
		{
			tap(newVersionCancel);
    	
		}	
		if(elementExists(virusNotification))
		{
			tap(virusNotification);
    	
		}	
		
    	waitFor(3);
    	screenshot(); // 1.cap login screen   
    	
    	try {
    		if(!elementExists(strFreqUsedApp)) {throw new Exception();}
    		writelog("Open App-ok");
		}catch(Exception e) {	
			saveScreenCapture();
			writelog("Open App-fail");
			throw e;
		}
    }

	@Given("click login Button$")
	public void login() throws Exception {
		try {

			List<Map<String, String>> data2 = getExcelAllRowData("HAGOUser");
			int size = data2.size();
			String hagoUsername = "";
			String hagoEncryptedPassword = "";
			String hagoPassword = "";
			for (int i = 0; i < size; i++) {
				Map<String, String> data = data2.get(i);
				hagoUsername = data.get("username");
				hagoEncryptedPassword = data.get("password");
			}

			if (HAGoCommon.language.equals(Const.english)) {
				elementDefinition("hagoE");
			} else {
				elementDefinition("hagoC");
			}
			tap(login);
			sendkeys(username, hagoUsername, true, true);
			sendkeys(password, hagoPassword, true, true);
			waitFor(3);
			tap(login2);
			waitFor(3);
			if (elementExists(useTouchIDNo)) {
				tap(useTouchIDNo);
				// tap(touchIDEnableLaterMsg);
			}
			waitFor(3);
			if (elementExists(otp1)) {
				mobileController.sendKeys("1");
				mobileController.sendKeys("1");
				mobileController.sendKeys("1");
				mobileController.sendKeys("1");
				mobileController.sendKeys("1");
				mobileController.sendKeys("1");
			}
			waitFor(3);
			screenshot(); // 1.cap login screen

			if (!elementExists(profile)) {
				throw new Exception(profile + ": not found");
			}
			writelog("Login-ok");
		} catch (Exception e) {
			saveScreenCapture();
			writelog("Login-fail");
			throw e;
		}
    }
	
	@Then("switch Language To Chinese$")
	public void switchLanguageToChinese() throws Exception{
		waitFor(3);
		if(elementExists(moreC))
		{
			HAGoCommon.language="Chinese"; 
			return;
		}	
		
		tap(moreE);
		waitFor(3);
		tap(moreLanguageOptionE);
		waitFor(3);
		tap(moreLanguageOptionSelectChineseE);
		waitFor(3);
		tap(homeC);
    	waitFor(3);
    	screenshot(); // 1.cap login screen
    	HAGoCommon.language="Chinese";    	
    	writelog("switchLanguageToChinese-ok");  	
	}
	
	@Then("switch Language To English$")
	public void switchLanguageToEnglish() throws Exception{

		waitFor(3);
		if(elementExists(moreE))
		{
			HAGoCommon.language="English"; 
			return;
		}
		tap(moreC);
		tap(moreLanguageOptionC);
		waitFor(3);
		tap(moreLanguageOptionSelectEnglishC);
		waitFor(3);
		tap(homeE);
    	waitFor(3);
    	screenshot(); // 1.cap login screen
    	
    	HAGoCommon.language="English";
	}
		
	@Then("click profile Button$")
	public void profile() throws Exception {
		tap(profile);
    	waitFor(3);
    	screenshot(); // 2.cap profile screen		
    	writelog("profile-ok");  	
	}


	@Then("click editEmail Button$")
	public void editEmail() throws Exception {
			tap(emailEdit);
//	    	sendkeys(emailEditPassword,"Appium2019");
			sendkeys(emailEditPassword,"hago$pwd2131");
	    	hideKeyboard();
	    	tap(emailEditConfirm);
	    	waitFor(3);
	    	sendkeys(emailEditNewEmail,"wfc543@ha.org.hk");
	    	hideKeyboard();
	    	tap(emailEditSave);
	    	waitFor(3);
	    	tap(emailEditConfirmLater);
	    	waitFor(3);
	    	screenshot(); // 3. cap edit email screen
	    	tap(emailEditChangeNotEffective);	
	    	tap(profileBackSymbol);	
	    	writelog("editEmail-ok");  	
	}

	@Then("click editMobile Button$")
	public void editMobile() throws Exception {
			tap(profile);
	    	waitFor(3);
	    	screenshot(); // 4.cap edit mobile number screen	   
	}

	@Then("click logout Button$")
	public void logout() throws Exception {	
	    try {
	    	waitFor(3);	
	    	tap(logout);
	    	tap(logoutConfirmYes);
	    	waitFor(3);	    	
	    	screenshot(); // 5. click log out screen	
	    	writelog("Logout-ok");
		}
		catch(Exception e) {	
			saveScreenCapture();
			writelog("Logout-fail");
			throw new Exception();
		}
	}

	
	@Then("click Pay HA")
	public void clickPayHA()throws Exception {
		try {
			tap(payhaIcon);
			waitFor(3);
			tap(payhaView);
			waitFor(3);
			if (!elementExists(payhaTotal)) {
				throw new Exception(payhaTotal+ ": not found");
			}

			mobileController.backOrSwipeRight();
			waitFor(3);
			mobileController.backOrSwipeRight();
			waitFor(3);
			
		}	catch(Exception e) {	
			saveScreenCapture();
			writelog("view payment fail");
			throw e;
		}
	}
	
	@Then("click MyAppointments$")
	public void myAppointments() throws Exception {
		try {
			tap(appMyappt);
			waitFor(3);
			tap(tabPast);
			waitFor(3);
			tap(btnPastShowCancelRescheduleRecord);
			waitFor(3);	    	
	    	screenshot(); //screen cap past appointment
			tap(tabFuture);
			waitFor(3);
			tap(btnFutureShowCancelRescheduleRecord);
	    	
	    	waitFor(3);	    	
	    	screenshot(); // screen cap future appointment
	    	tap(btnMyapptBackSymbol);
	    	
	    	writelog("MyAppts-ok");
		}
		catch(Exception e) {	
			saveScreenCapture();
			writelog("MyAppts-fail");
			throw new Exception();
		}
	}

	@Then("click BookHA after login$")
	public void bookHAAfterLogin() throws Exception {
		//tap(appMyappt);
		try {
//			writelog("Open bookHA - starts");
//			writelog("Open bookHA - bookha:"+bookhaAfterLogin);
			waitFor(10);	 
			tap(bookhaAfterLogin);
//			writelog("Open bookHA - after tap bookhaAfterLogin");
	    	waitFor(10);	    	
	    	screenshot(); // screen cap future appointment
	    	if(!elementExists(getElDef("mBookingE.submitApplication")) && !elementExists(getElDef("mBookingC.submitApplication")) ) {throw new Exception();}
//	    	writelog("Open bookHA - before tap back");
//	    	writelog("Open bookHA - btnBookhaBackSymbol:"+getElDef("hago.btnBookhaBackSymbol"));
	    	tap(getElDef("hago.btnBookhaBackSymbol"));
	    	
	    	writelog("BookHA-ok");
		}
		catch(Exception e) {	
			saveScreenCapture();
			writelog("BookHA-fail");
			throw new Exception();
		}
	}
	
}




 
