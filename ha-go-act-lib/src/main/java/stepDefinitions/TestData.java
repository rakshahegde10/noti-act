package stepDefinitions;

public class TestData {
    public static final String haNotificationHeaderEnglish = "Hospital Authority";
    public static final String haNotificationHeaderChinese = "醫院管理局";
    public static final String firstHaMessage = "HA General Message";
    public static final String secondHaMessage = "Since the Hong Kong Observatory has issued Tropical Cyclone Signal No. 8 or above / Black Rainstorm Warning Signal / “Extreme Conditions”,the outpatient service is suspended";
    public static final String thirdHaMessage = "由於天文台發出八號或以上熱帶氣旋警告信號 / 黑色暴雨警告信號 /「極端情況」公布，門診服務現已暫停";
}
