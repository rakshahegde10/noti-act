package stepDefinitions;

public class Const {
    public static final String english = "English";
    public static final String chinese = "Chinese";
    public static final String excelSheetName = "HAGO";
    public static final int defaultRetryLoopClickTimes = 10;
    public static final String hagoE = "hagoE";
    public static final String hagoC = "hagoC";
    public static final String myAppointments = "My Appointments";
    public static final String payha = "PayHa";
    public static final String payhaC = "PayHaC";
    public static final String rehab = "Rehab";
    public static final String telecare = "Telecare";
    public static final String generalMessages = "General Messages";
}
