package stepDefinitions;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import framework.operations.AppiumOperation;
import framework.operations.Operation;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import static io.appium.java_client.touch.offset.PointOption.point;
import utility.Hook;

public class HaGoCommonController {

	protected MobileController mobileController;	
	
	public HaGoCommonController() throws Exception {
		mobileController = new MobileController();	
	}

	public String getElDef(String elementDefineValue) throws Exception {
		return mobileController.getElDef(elementDefineValue);
	}
	
	public String replaceValue(String originalValue) {
		return originalValue.replace("'", "''");
	}
	
	public void runInBackground() {
		Duration backgroundDuration = Duration.ofSeconds(5);
		mobileController.getOperation().runAppInBackground(backgroundDuration);
	}

	public void closeApp() {
		Hook.<AppiumOperation>getOperation().closeApp();
		try {Hook.<AppiumOperation>getOperation().runAppInBackground(Duration.ofSeconds(1));} catch (Exception e){}
	}
	
	public void outputText(StringBuilder stringBuilder, String fileName) throws Exception {
		File file = new File(fileName);
		BufferedWriter writer=null;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(stringBuilder.toString());
		} finally {
			if(writer!=null) writer.close();
		}
	}
	

	public void swipeUp() throws Exception {
		mobileController.swipeUp();		
//		saveScreenCapture();
	}
	
	public void swipeDown() throws Exception {
		mobileController.swipeDown();
//		saveScreenCapture();
	}
	
	public void tap(String element) throws Exception {
		if (element.contains("coordinator"))
			tapByCoordinator(element);
		else
			mobileController.tap(element);
//		saveScreenCapture();
	}
	
	public void longTap(String element) throws Exception {
		mobileController.longTap(element);
//		saveScreenCapture();
	}

	public void tapIfExists(String element) throws Exception {
		if (elementExists(element)) {
			mobileController.tap(element);
		}
	}
	
	public AppiumOperation getOperation() throws Exception {
		return mobileController.getOperation();
	}
	
	public void waitFor(int waitTime) {
		mobileController.waitFor(waitTime);
	}

	public void goBack() throws Exception {
		mobileController.backOrSwipeRight();
	}

	public void print(String log) throws Exception {
		System.out.println(log);
	}

	public void assertElementSize(int expectValue, String element) throws Exception {
		mobileController.assertEqualsInt(expectValue, mobileController.getElementsSize(element));
	}
	
	public void assertRecordsSize(int expectValue, List<Map<String, String>> records) throws Exception {
		mobileController.assertEqualsInt(expectValue, records.size());
	}
	
	public void assertElementText(String expectValue, String element) throws Exception {
		mobileController.assertEqualsTextWithTrim(expectValue, mobileController.getElementText(element));
	}
	
	public void assertTextEqual(String expectValue, String actualValue) throws Exception {
		mobileController.assertEqualsTextWithTrim(expectValue, actualValue);
	}
	
	public String getCurrentAppName() {
		return mobileController.getCurrentAppName();
	}
	
	public void sendKeys(String value) throws Exception {
		mobileController.sendKeys(value);
//		saveScreenCapture();
	}
	
	public void sendkeys(String element, String value) throws Exception {
		mobileController.sendkeys(element, value);
//		saveScreenCapture();
	}
	public void sendkeys(String element, String value, boolean clearText) throws Exception {
		mobileController.sendkeys(element, value, clearText);
//		saveScreenCapture();
	}
	
	public void sendkeys(String element, String value, boolean clearText, boolean hideKeyoard) throws Exception {
		mobileController.sendkeys(element, value, clearText, hideKeyoard);
//		saveScreenCapture();
	}
	
	public void pressKey(Keys key) throws Exception {
		mobileController.pressKey(key);
	}
	
	public void disableKeyboard() throws Exception {
		mobileController.hideKeyboard();
	}
	
	public boolean isAndroid() {
		return mobileController.isAndroid();
	}
	
	public boolean isIOS() {
		return mobileController.isIOS();
	}
	
	public boolean elementExists(String element) {
		return mobileController.elementExists(element);
	}
	public boolean elementExists(String element, Integer retry) {
		return mobileController.elementExists(element, retry);
	}
	
	public String getElementText(String element) throws Exception {
		return mobileController.getElementText(element);
	}
	
	public int getElementsSize(String element) throws Exception {
		return mobileController.getElementsSize(element);
	}

	public String getXpathWithIndex(String xpath, int index) throws Exception {
		return "(" + xpath + ")["+ index + "]";
	}

	public String getTextInXpath(String value, boolean iosUseNameTag) throws Exception {
		return isAndroid() ? "//*[contains(@text, '" + value + "')]" : iosUseNameTag ? "//*[contains(@name, '" + value + "')]" : "//*[contains(@label, '" + value + "')]";
	}
	
	public void saveScreenCapture() throws IOException {
		String isCapture = mobileController.readPropertiesFile("runtime-setting.properties", "isScreenCapture");	
		String folderName="output/image";
		if(isCapture.equals("1")) {
			WebDriver driver=Hook.getDriver();
			File file=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			String screenName=new SimpleDateFormat("yyyyMMdd-HHmmsss").format(new Date())+".jpg";		
			FileUtils.copyFile(file, new File(folderName+"/"+screenName));
		}		
	}
	
	public void screenshot() throws Exception {
		mobileController.screenshot();
	}
	
	public WebElement swipeWhileElementNotDisplayed(String elementName, int rounds, String direction, double startPercentage, double endPercentage, double anchorPercentage, boolean tap) throws Exception {
		return mobileController.swipeWhileElementNotDisplayed(elementName, rounds, direction, startPercentage, endPercentage, anchorPercentage, tap);
	}

	public WebElement getElement(String elementName) throws Exception {
		return mobileController.findElementBy(elementName);
	}

	public int getXLocation(WebElement element) throws Exception {
		return element.getLocation().x;
	}

	public int getYLocation(WebElement element) throws Exception {
		return element.getLocation().y;
	}

	public String getFirstXpathInElementNameFormat(String value) throws Exception {
		return convertXpathIntoElementName(getXpathWithIndex(getTextInXpath(value, false), 1));
	}

	public String convertXpathIntoElementName(String xpath) throws Exception {
		return "xpath: " + xpath;
	}

	public void hideKeyboard() throws Exception{
		mobileController.hideKeyboard();
	}

	public void waitForAppearance(String elementName) throws Exception {
		int appearanceCount = 0;
		while (elementExists(elementName)) {
			waitFor(1);
			appearanceCount++;
			if (appearanceCount > Const.defaultRetryLoopClickTimes) {
				break;
			}
		}
	}

	public void waitForDisappearance(String elementName) throws Exception {
		int disappearCount = 0;
		while (elementExists(elementName)) {
			waitFor(1);
			disappearCount++;
			if (disappearCount > Const.defaultRetryLoopClickTimes) {
				break;
			}
		}
	}

	// written by jw
	public void tapByCoordinator(int x, int y) throws Exception{		
		CommonOperationDriverController cdc = new CommonOperationDriverController();
		WebDriver driver=Hook.getDriver();
		TouchAction touchAction=new TouchAction((PerformsTouchActions) driver);
		touchAction.tap(point(x, y)).perform();
	}

	public void tapByCoordinator(String coordinator) throws Exception{
		// coordinator: 100,200
		int x=0;
		int y=0;
		x=Integer.parseInt(coordinator.substring(coordinator.indexOf(":")+1, coordinator.indexOf(",")).replaceAll("\\s+", ""));
		y=Integer.parseInt(coordinator.substring(coordinator.indexOf(",")+1));
		writelog("tapByCoordinator: X: "+x);
		writelog("tapByCoordinator: Y: "+y);
		CommonOperationDriverController cdc = new CommonOperationDriverController();
		WebDriver driver=Hook.getDriver();
		TouchAction touchAction=new TouchAction((PerformsTouchActions) driver);
		touchAction.tap(point(x, y)).perform();
	}
	
	public void swipe(double startPercentage, double endPercentage, double anchorPercentage) throws Exception{
		mobileController.verticalSwipeByPercentages(startPercentage, endPercentage, anchorPercentage);
	}
	
	public String getPageSource() {
		WebDriver driver=Hook.getDriver();
		  return driver.getPageSource();
		}
	
	
	public void writelog(String content) {
		try
		{
			String testEnv = System.getProperty("test.env");
			String today = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
			String filename = "log/stdout."+today+"."+testEnv+".log"; // for Jenkins, the '/' need to be left slash
			if (!new File("log").exists())
			{				
				new File("log").mkdir();				
			}
			FileWriter fw = new FileWriter(filename,true);
//			String currentDatetime = new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(Calendar.getInstance().getTime());
			String currentDatetime = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			fw.write(currentDatetime+"|"+content+"\n");
			fw.close();

			
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}
	}

	public void writeMBNumber(String content,boolean append) {
		try
		{			
			String filename = "MBNumber.log"; // for Jenkins, the '/' need to be left slash
//			String filename = "/Volumes/wfc542/MBNumber.log"; // for Jenkins, the '/' need to be left slash
			FileWriter fw = new FileWriter(filename,append);
			fw.write(content);
			fw.close();
			
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}
	}

	//hkid: strReturn[0]
	//MBNumber: strReturn[1]
//	public String[] readMBNumber() throws Exception {
////			String filename = "MBNumber.log";
//		String filename = "/Volumes/wfc542/MBNumber.log";
//			String[] strReturn;  
//			File file = new File(filename); 
//			  
//			  BufferedReader br = new BufferedReader(new FileReader(file)); 
//			  
//			  String st=br.readLine(); 
//			  writelog("MBNumber.log-content:"+st);
//			  br.close();
//			  strReturn = st.split("-");
//			  return strReturn;
//		
//	}
	public ArrayList readMBNumber() throws Exception {
		String filename = "MBNumber.log";
//	String filename = "/Volumes/wfc542/MBNumber.log"; // i drive
		ArrayList alReturn=new ArrayList();  
		File file = new File(filename); 
		
//		boolean exists = false;
//		int iWait = 0;
//		do
//			{
//			exists= file.exists();
//			if (exists) break;
//			waitFor(1);
//			writelog("file not yet exist. wait " + iWait + " seconds...");
//			iWait ++;
//			}while(!exists);		
		  
		  BufferedReader br = new BufferedReader(new FileReader(file)); 
		  
		  String strLine;
		  String[] arrLine;
		  while ((strLine=br.readLine())!=null)
		  {
			  arrLine = strLine.split("-");
			  writelog("MBNumber.log-content:"+strLine);
			  writelog("MBNumber.log-1st:"+arrLine[0]);
			  writelog("MBNumber.log-2nd:"+arrLine[1]);
			  alReturn.add(arrLine);
					
				}	
		  return alReturn;
	}
	public String getMBNumber()
	{
		String strMBNumber = "";
		String strTemp = getPageSource();
		//writelog(strTemp);
		if((strTemp.indexOf("MB"))>0)
		{
			strMBNumber = strTemp.substring(strTemp.indexOf("MB"),strTemp.indexOf("MB")+9);
			writelog("MBNumber: "+strMBNumber);
		}
		else writelog("No MBNumber found!!!");
		return strMBNumber;
	}
	public List<Map<String,String>> getExcelAllRowData (String sheetName)throws Exception
	{
		return this.mobileController.getExcelAllRowData(sheetName);
	}
}



