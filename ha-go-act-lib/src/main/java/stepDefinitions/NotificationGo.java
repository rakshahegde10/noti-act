package stepDefinitions;

import java.util.List;
import java.util.Map;

import com.sun.jna.Platform;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import framework.driver.Log;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NotificationGo extends HaGoCommonController {
	private String haAdjustServiceProvisionToFocusOnEpidemicPopUpOkButton;
	private String confirmRootedOrJailbreak;
	private String skip;
	private String jailbreakConfirm;
	private String virusOK;
	private String newVersionCancel;
	private String virusNotification;
	private String strFreqUsedApp;
	private String notificationIcon;
	private String notificationIconAndroidOptional;
	private String notificationHeader;
	private String notificationIconAfterLogin;
	private String notificationIconBeforeLogin;
	private int count;
	private String login;
	private String backFromContentBtn;
	private String usernameTextField;
	private String passwordTextField;
	private String submitLogin;
	private String notificationCenterBackBtn;
	private String touchIdNegativeBtn;
	private String touchIdOkBtn;
	private String authorizeTouchIdHeader;
	private String authorizePageBackBtn;
	private String verifyMobileNumberPageHeader;
	private String notificationIconAfterLoginAndroidOptional;
	private String leaveActivationPageBtn;
	private String basicMemberNotiIcon;
	private String myAppointmentMiniAppVerification;
	private String payHaMiniAppVerification;
	private String weatherBtn;
	private String rehabMiniAppVerification;
	private String otpOne;
	private String otpTwo;
	private String otpThree;
	private String otpFour;
	private String otpFive;
	private String otpSix;
	private String logoutBtn;
	private String confirmLogoutBtn;
	private String authorizeFaceIdHeader;
	private String authorizeFaceIdPageBackBtn;
	private String multipleDeviceBtn;
	private String receivePushBtn;
	private String switchDeviceBtn;
	private String fontSizeOption;
	private String more;
	private String home;
	private String bigText;
	private String smallText;
	private String mediumText;
	private String notificationCountBadgeOne;
	private String notificationCountBadgeZero;
	private String notificationCountBadgeThree;
	private String notificationRedBadge;
	private String searchIcon;
	private String searchBar;
	private String myAppointmentsBackBtn;
	private String rehabBackBtn;
	private String payhaBackBtn;
	private String notificationSearchResults;
	private String collapsedIcon;
	private String expandedIcon;
	private String highlightedWord;
	private String clearSearchString;
	private String filterDropdownList;
	private String filterDropdownListRehab;
	private String filterDropdownListPayHA;
	private String filterDropdownListHA;
	private String filterListPayhaOne;
	private String filterListPayhaTwo;
	private String filterListRehab;
	private String filterRehabLabel;
	private String filterPayHALabel;
	private String filterHALabel;
	private String filterListHA;
	private String undercareUserIcon;
	private String undercareUserIcons;
	private String undercareUser;
	private String undercareUserBellIcon;
	private String notificationRedBadgeDisappears;
	private String undercareUserIos;
	private String undercareUserIosNC;
	private String notificationIconIndicator;
	
	// switch language: Chinese to English (C2E)
	private String moreC;
	private String moreLanguageOptionC;
	private String moreLanguageOptionSelectEnglishC;
	private String homeC;
	// switch language: English to Chinese (E2C)
	private String moreE;
	private String moreLanguageOptionE;
	private String moreLanguageOptionSelectChineseE;
	private String homeE;
	
	public NotificationGo() throws Exception {
		if (isAndroid()) {
			notificationIconAndroidOptional = getElDef("hago.main.page.notification.icon.before.login.android.optional");
			notificationIconAfterLoginAndroidOptional = getElDef("hago.main.page.notification.icon.after.login.android.optional");
			otpOne = getElDef("hago.otp1");
			otpTwo = getElDef("hago.otp2");
			otpThree = getElDef("hago.otp3");
			otpFour = getElDef("hago.otp4");
			otpFive = getElDef("hago.otp5");
			otpSix = getElDef("hago.otp6"); 
		}
		if (isIOS()) {
		}
		basicMemberNotiIcon = getElDef("hago.main.page.notification.icon.basic.member");
		virusOK = getElDef("hago.virusOK");
		virusNotification = getElDef("hago.virusNotification");
		strFreqUsedApp = getElDef("hago.strFreqUsedApp");
		count = 0;
		//switch language: C2E
		moreC = getElDef("hago.moreC");
		moreLanguageOptionC = getElDef("hago.moreLanguageOptionC");
		moreLanguageOptionSelectEnglishC= getElDef("hago.moreLanguageOptionSelectEnglishC");
		homeE = getElDef("hago.homeE");

		//switch language: E2C
		moreE = getElDef("hago.moreE");
		moreLanguageOptionE = getElDef("hago.moreLanguageOptionE");
		moreLanguageOptionSelectChineseE = getElDef("hago.moreLanguageOptionSelectChineseE");
		homeC = getElDef("hago.homeC");
	}

	public void elementDefinition(String strStartsWith) throws Exception {
		if (isIOS()) {
			backFromContentBtn = getElDef(strStartsWith + ".back.from.notification.btn");
			authorizePageBackBtn = getElDef(strStartsWith + ".authorise.touchid.page.back.btn.ios.optional");
			authorizeFaceIdPageBackBtn = getElDef(strStartsWith + ".authorise.faceid.page.back.btn.ios.optional");
		}
		multipleDeviceBtn = getElDef(strStartsWith + ".popup.multiple.device.btn.ok");
		receivePushBtn = getElDef(strStartsWith + ".popup.receivepush.btn.confirm");
		switchDeviceBtn = getElDef(strStartsWith + ".popup.switchDevice.btn.confirm");
		home = getElDef(strStartsWith + ".home");
		mediumText = getElDef(strStartsWith + ".mediumText");
		bigText = getElDef(strStartsWith + ".bigText");
		smallText = getElDef(strStartsWith + ".smallText");
		more = getElDef(strStartsWith + ".more");
		fontSizeOption = getElDef(strStartsWith + ".fontSizeOption");
		newVersionCancel = getElDef(strStartsWith + ".newVersionCancel");
		weatherBtn = getElDef(strStartsWith + ".popup.weather.btn.ok");
		notificationCenterBackBtn = getElDef(strStartsWith + ".notification.page.back.btn");
		rehabMiniAppVerification = getElDef(strStartsWith + ".rehab.content");
		confirmLogoutBtn = getElDef(strStartsWith + ".logout.btn.confirm");
		logoutBtn = getElDef(strStartsWith + ".logout.btn");
		basicMemberNotiIcon = getElDef(strStartsWith + ".main.page.notification.icon.basic.member");
		myAppointmentMiniAppVerification = getElDef(strStartsWith + ".my.appointments.content");
		payHaMiniAppVerification = getElDef(strStartsWith + ".payha.content");
		notificationIcon = getElDef(strStartsWith + ".main.page.notification.icon.before.login");
		jailbreakConfirm = getElDef(strStartsWith + ".jailbreakConfirm");
		leaveActivationPageBtn = getElDef(strStartsWith + ".main.page.activation.page.leave.btn");
		touchIdNegativeBtn = getElDef(strStartsWith + ".main.page.touchid.negative.btn");
		touchIdOkBtn = getElDef(strStartsWith + ".main.page.touchid.ok.btn");
		verifyMobileNumberPageHeader = getElDef(strStartsWith + ".verify.mobile.number.header");
		authorizeTouchIdHeader = getElDef(strStartsWith + ".main.page.touchid.header");
		authorizeFaceIdHeader = getElDef(strStartsWith + ".main.page.faceid.header");
		notificationIconAfterLogin = getElDef(strStartsWith + ".main.page.notification.icon.after.login");
		notificationIconBeforeLogin = getElDef(strStartsWith + ".main.page.notification.icon.before.login");
		notificationHeader = getElDef(strStartsWith + ".notification.page.header");
		haAdjustServiceProvisionToFocusOnEpidemicPopUpOkButton = getElDef(strStartsWith + ".popup.combatEpidemic.btn.ok");
		skip = getElDef(strStartsWith + ".skip");
		login = getElDef(strStartsWith + ".login");
		usernameTextField = getElDef(strStartsWith + ".username");
		passwordTextField = getElDef(strStartsWith + ".password");
		submitLogin = getElDef(strStartsWith + ".login2");
		notificationCountBadgeOne = getElDef(strStartsWith + ".unread.notification.count.badge.one");
		notificationCountBadgeZero = getElDef(strStartsWith + ".unread.notification.count.badge.zero");
		notificationCountBadgeThree = getElDef(strStartsWith + ".unread.notification.count.badge.three");
		notificationRedBadge = getElDef(strStartsWith + ".unread.notification.red.dot");
		notificationRedBadgeDisappears = getElDef(strStartsWith + ".unread.notification.red.dot.disappears");
		searchIcon = getElDef(strStartsWith + ".click.search.icon");
		searchBar = getElDef(strStartsWith + ".display.search.bar");
		myAppointmentsBackBtn = getElDef(strStartsWith + ".myappointments.page.back.btn");
		rehabBackBtn = getElDef(strStartsWith + ".tap.rehab.popup");
		payhaBackBtn = getElDef(strStartsWith + ".payha.page.back.btn");
		notificationSearchResults = getElDef(strStartsWith + ".verify.notification.search.bar"); 
		collapsedIcon = getElDef(strStartsWith + ".collapsed.icon"); 
		expandedIcon = getElDef(strStartsWith + ".expanded.icon");
		highlightedWord = getElDef(strStartsWith + ".highlighted.word");  
		clearSearchString = getElDef(strStartsWith + ".clear.search.string"); 
		filterDropdownList = getElDef(strStartsWith + ".filter.dropdown.list");
		filterDropdownListRehab = getElDef(strStartsWith + ".filter.dropdown.list.rehab"); 
		filterDropdownListPayHA = getElDef(strStartsWith + ".filter.dropdown.list.payha"); 
		filterDropdownListHA = getElDef(strStartsWith + ".filter.dropdown.list.ha"); 
		filterListPayhaOne = getElDef(strStartsWith + ".filter.payha.message1"); 
		filterListPayhaTwo = getElDef(strStartsWith + ".filter.payha.message2"); 
		filterListRehab = getElDef(strStartsWith + ".filter.rehab.message"); 
		filterListHA = getElDef(strStartsWith + ".filter.ha.message"); 
		filterRehabLabel = getElDef(strStartsWith + ".filter.rehab.label"); 
		filterPayHALabel = getElDef(strStartsWith + ".filter.payha.label"); 
		filterHALabel = getElDef(strStartsWith + ".filter.ha.label"); 
		undercareUserIcon = getElDef(strStartsWith + ".undercare.user.icon");
		undercareUserIcons = getElDef(strStartsWith + ".undercare.user.icons");
		undercareUser = getElDef(strStartsWith + ".undercare.user");
		undercareUserIos = getElDef(strStartsWith + ".undercare.user.name");
		undercareUserIosNC = getElDef(strStartsWith + ".undercare.user.name.nc");
		undercareUserBellIcon = getElDef(strStartsWith + ".undercare.user.notification.center.icon");
		notificationIconIndicator = getElDef(strStartsWith + ".notification.icon.indicator");   
	}

	public void leaveAuthorizeTouchIdPage() throws Exception{
		if (isIOS()) {
			/// goBack() function does not work here for an unknown reason, so we have to make a workaround here
			if (elementExists(authorizeTouchIdHeader)) {
				loopClickUntilGone(authorizePageBackBtn, authorizeTouchIdHeader);
			} else if (elementExists(authorizeFaceIdHeader)) {
				loopClickUntilGone(authorizeFaceIdPageBackBtn, authorizeFaceIdHeader);
			}
		} else {
			if (elementExists(touchIdNegativeBtn)) {
				loopClickUntilSelfGone(touchIdNegativeBtn);
				loopClickUntilSelfGone(touchIdOkBtn);
			}
		}
	}

	public void checkExistence(String xpath) throws Exception {
		if (!elementExists(xpath, 10)) {
			throw new Exception("Element does not exist");
		}
	}

	public String getGeneralMessageHeader() throws Exception {
			return HAGoCommon.language.equals(Const.english) ? TestData.haNotificationHeaderEnglish : TestData.haNotificationHeaderChinese;
	}

	public void checkExistence(String xpath, int retryTime) throws Exception {
		if (!elementExists(xpath, retryTime)) {
			throw new Exception("Element does not exist");
		}
	}

	public void checkNonExistence(String xpath) throws Exception {
		if (elementExists(xpath, 10)) {
			throw new Exception("Element exists");
		}
	}

	public void checkNonExistence(String xpath, int retryTime) throws Exception {
		if (elementExists(xpath, retryTime)) {
			throw new Exception("Element exists");
		}
	}

	public void loopClickUntilSelfGone(String clickTarget) throws Exception {
		count = 0;
		while (elementExists(clickTarget)) {
			tap(clickTarget);
			mobileController.waitFor(1);
			count++;
			if (count > Const.defaultRetryLoopClickTimes) {
				throw new Exception("Failed to dismiss: " + clickTarget);
			}
			waitFor(2);
		}
	}

	public void loopClickUntilGone(String clickTarget, String satisfiedTarget) throws Exception {
		count = 0;
		while (elementExists(satisfiedTarget)) {
			tap(clickTarget);
			mobileController.waitFor(1);
			count++;
			if (count > Const.defaultRetryLoopClickTimes) {
				throw new Exception("Failed to click: " + clickTarget);
			}
			waitFor(2);
		}
	}

	public void loopClickUntilFound(String clickTarget, String satisfiedTarget) throws Exception {
		count = 0;
		while (!elementExists(satisfiedTarget)) {
			tap(clickTarget);
			mobileController.waitFor(1);
			count++;
			if (count > Const.defaultRetryLoopClickTimes) {
				throw new Exception("Failed to click: " + clickTarget);
			}
			waitFor(2);
		}
	}

	public void loopClickUntilFound(String clickTarget, String satisfiedTarget, int retryTimes) throws Exception {
		count = 0;
		while (!elementExists(satisfiedTarget)) {
			tap(clickTarget);
			count++;
			if (count > retryTimes) {
				throw new Exception("Failed to click: " + clickTarget);
			}
		}
	}

	public void loopSwipeUntilFound(String target, boolean swipeUp) throws Exception {
		count = 0;
		while (!elementExists(target)) {
			if (isAndroid()) {
				if (swipeUp) {
					swipeUp();
				} else {
					swipeDown();
				}
			} else {
				if (swipeUp) {
					mobileController.verticalSwipeByPercentages(0.5, 0.6, 0.5);
				} else {
					mobileController.verticalSwipeByPercentages(0.6, 0.5, 0.5);
				}
			}
			print("Swipe for " + count + " times");
			mobileController.waitFor(1);
			count++;
			if (count > Const.defaultRetryLoopClickTimes) {
				throw new Exception("Failed to swipe and find: " + target);
			}
		}
	}

	public String getFilterValue(String indicator) throws Exception {
		Log.info("Received indicator: " + indicator);
		boolean isValidFormat = indicator.contains(".") && indicator.contains("HAGON-");
		if (!isValidFormat) {
			return indicator;
		}
		List<Map<String, String>> data = getExcelAllRowData(Const.excelSheetName);
		return indicator.split("\\.")[0];
	}

	public String getValueFromExcel(String indicator) throws Exception {
		Log.info("Received indicator: " + indicator);
		boolean isValidFormat = indicator.contains(".") && indicator.contains("HAGON-");
		if (!isValidFormat) {
			return indicator;
		}
		if (isAndroid() && indicator.contains(".username")) {
		    indicator = indicator.replace(".username", ".androidUsername");
		} else {
		    indicator = indicator.replace(".username", ".iphoneUsername");
		}
		if (isAndroid() && indicator.contains(".password")) {
		    indicator = indicator.replace(".password", ".androidPassword");
		} else {
		    indicator = indicator.replace(".password", ".iphonePassword");
		}
		List<Map<String, String>> data = getExcelAllRowData(Const.excelSheetName);
		int size = data.size();
		Integer targetIndex = null;
		String firstPartOfIndicator = indicator.split("\\.")[0];
		String secondPartOfIndicator = indicator.split("\\.")[1];
		for (int i = 0; i < size; i++) {
			Map<String, String> insideData = data.get(i);
			if (insideData.containsValue(firstPartOfIndicator)) {
				targetIndex = i;
				break;
			}
		}
		if (targetIndex == null) {
			return indicator;
		} else {
			Map<String, String> insideData = data.get(targetIndex);
			Log.info("Fetched value from excel: " + insideData.get(secondPartOfIndicator));
			return insideData.get(secondPartOfIndicator);
		}
	}

	public void switchFontSize(String value) throws Exception {
		String target = "";
		String defaultValue = "";
		if (isIOS()) {
//			fontSizeOption might appear in home page, so it cant be the satisfy target for the below function
			if (HAGoCommon.language.equalsIgnoreCase(Const.english)) {
				loopClickUntilFound(more, "xpath: (//XCUIElementTypeOther[@name='More'])[2]");
			} else {
				loopClickUntilFound(more, "xpath: (//XCUIElementTypeOther[@name='更多'])[2]");
			}
		} else {
			loopClickUntilFound(more, fontSizeOption);
		}
		screenshot();
		if (value.equalsIgnoreCase("Large")) {
			target = bigText;
			if (HAGoCommon.language.equalsIgnoreCase(Const.english)) {
				defaultValue = "xpath: //XCUIElementTypeOther[@name='Font Size Large']";
			} else {
				defaultValue = "xpath: //XCUIElementTypeOther[@name='字體大小 大']";
			}
		}
		if (value.equalsIgnoreCase("Small")) {
			target = smallText;
			if (HAGoCommon.language.equalsIgnoreCase(Const.english)) {
				defaultValue = "xpath: //XCUIElementTypeOther[@name='Font Size Small']";
			} else {
				defaultValue = "xpath: //XCUIElementTypeOther[@name='字體大小 小']";
			}
		}
		if (value.equalsIgnoreCase("Medium")) {
			target = mediumText;
			if (HAGoCommon.language.equalsIgnoreCase(Const.english)) {
				defaultValue = "xpath: //XCUIElementTypeOther[@name='Font Size Medium']";
			} else {
				defaultValue = "xpath: //XCUIElementTypeOther[@name='字體大小 中']";
			}
		}
		if (target.isEmpty()) {
			throw new Exception("Font size target cannot be empty. Please give this function an argument");
		}
		if ((!elementExists(target) && isAndroid()) || (isIOS() && !elementExists(defaultValue))) {
			loopClickUntilFound(fontSizeOption, target);
			screenshot();
			tap(target);
		}
		loopClickUntilFound(home, login);
	}

	public void switchLanguageToChinese() throws Exception{
		loopClickUntilFound(moreE, moreLanguageOptionE);
		loopClickUntilFound(moreLanguageOptionE, moreLanguageOptionSelectChineseE);
		waitFor(3);
		loopClickUntilGone(moreLanguageOptionSelectChineseE, moreLanguageOptionE);
		elementDefinition(Const.hagoC);
		waitFor(3);
		loopClickUntilFound(homeC, login);
		waitFor(3);
		screenshot();
		waitFor(3);
		screenshot();
		HAGoCommon.language = Const.chinese;
	}

	public void switchLanguageToEnglish() throws Exception{
		waitFor(3);
		loopClickUntilFound(moreC, moreLanguageOptionC);
		loopClickUntilFound(moreLanguageOptionC, moreLanguageOptionSelectEnglishC);
		waitFor(3);
		loopClickUntilGone(moreLanguageOptionSelectEnglishC, moreLanguageOptionC);
		elementDefinition(Const.hagoE);
		waitFor(3);
		loopClickUntilFound(homeE, login);
		waitFor(3);
		screenshot();
		HAGoCommon.language = Const.english;
	}

	@Given("^I am on HA Go Mobile landing page in \"([^\"]*)\" version$")
	public void openHAGoApp(String appLanguage) throws Exception {
		try {
			elementDefinition(Const.hagoE);
			if (elementExists(haAdjustServiceProvisionToFocusOnEpidemicPopUpOkButton) || elementExists(skip) || elementExists(login) || elementExists(moreE) || elementExists(jailbreakConfirm) || elementExists(weatherBtn) || elementExists(newVersionCancel)) {
				HAGoCommon.language = Const.english;
			} else {
				HAGoCommon.language = Const.chinese;
				elementDefinition(Const.hagoC);
			}
			waitFor(5); /// This waiting is needed because skip button will exist on the screen for a very short time even if not the first time opening app
			// The ordering of the following four checking matters. Do not change it.
			if(elementExists(skip)) {
				loopClickUntilSelfGone(skip);
			}
			if (elementExists(jailbreakConfirm)) {
				loopClickUntilSelfGone(jailbreakConfirm);
			}
			if (elementExists(newVersionCancel)) {
				loopClickUntilSelfGone(newVersionCancel);
			}
			if (elementExists(haAdjustServiceProvisionToFocusOnEpidemicPopUpOkButton)) {
				loopClickUntilSelfGone(haAdjustServiceProvisionToFocusOnEpidemicPopUpOkButton);
			}
			if (elementExists(weatherBtn)) {
				loopClickUntilSelfGone(weatherBtn);
			}
			screenshot(); // 1.cap login screen
			waitFor(3);
			if (appLanguage.equals(Const.english)) {
				if (!elementExists(moreE)) {
					switchLanguageToEnglish();
				}
			} else {
				if (!elementExists(moreC)) {
					switchLanguageToChinese();
				}
			}
		 } catch (Exception e) {
			screenshot(); // 1.cap login screen
			saveScreenCapture();
			writelog("Open App-fail");
			throw e;
		}
    }

	@Given("^I leave activation page$")
	public void iLeaveActivationPage() throws Exception {
		tap(leaveActivationPageBtn);
		waitFor(3);
		screenshot();
	}

	@Given("^I have switched font size to \"([^\"]*)\"")
	public void iHaveSwitchedFontSize(String target) throws Exception {
		switchFontSize(target);
		screenshot();
	}

	@Given("^I navigate to login page$")
	public void iHaveNavigatedToLoginPage() throws Exception {
		loopClickUntilFound(login, submitLogin);
    	waitFor(3);
    	screenshot();
    }

	@When("^I navigate to Notification Center after login as a basic member$")
	public void INavigateToNotiCenterAsABasicMember() throws Exception {
		if (isAndroid()) {
			if (elementExists(notificationIconAfterLogin)) {
				loopClickUntilFound(notificationIconAfterLogin, notificationHeader);
			} else {
				loopClickUntilFound(basicMemberNotiIcon, notificationHeader);
			}
		} else {
			loopClickUntilFound(basicMemberNotiIcon, notificationHeader);
		}
		waitFor(3);
		screenshot(); // Cap screen in notification center
	}

//	@Given("^I navigate to Notification Center after login$")
//	public void iNavigateToNotificationCenterAfterLogin() throws Exception {
//		if (elementExists(notificationIconAfterLogin)) {
//  		    loopClickUntilFound(notificationIconAfterLogin, notificationHeader);			
//		} else {
//			loopClickUntilFound(notificationIconAfterLoginAndroidOptional, notificationHeader);
//		}
//		waitFor(3);
//		screenshot(); // Cap screen in notification center
//	}
	
	@Given("^I navigate to Notification Center after login with \"([^\"]*)\"$")
	public void iNavigateToNotificationCenterAfterLogin(String username) throws Exception {
		String filterValue = getFilterValue(username);
		String name;
		String xpath;
		if(isAndroid()) {
			if (elementExists(notificationIconAfterLogin)) {
	  		    loopClickUntilFound(notificationIconAfterLogin, notificationHeader);			
			} else {
				loopClickUntilFound(notificationIconAfterLoginAndroidOptional, notificationHeader);
			}
		} else {
			switch(username) {
			case "HAGON-1.username" :
			name = getValueFromExcel(filterValue + ".name");
			xpath = "(//XCUIElementTypeOther[@name=\"Hello " + name + "\"])[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther";
			loopClickUntilFound(xpath, notificationHeader);
			break;
			case "HAGON-2.username" :
			name = getValueFromExcel(filterValue + ".name");
			xpath= "(//XCUIElementTypeOther[@name=\"Hello " + name + "\"])[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther";
			loopClickUntilFound(xpath, notificationHeader);
			break;
			case "HAGON-3.username" :
			name = getValueFromExcel(filterValue + ".name");
			xpath= "(//XCUIElementTypeOther[@name=\"Hello " + name + "\"])[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther";
			loopClickUntilFound(xpath, notificationHeader);
			break;
			}
		}
		waitFor(3);
		screenshot(); // Cap screen in notification center
	}

	
	@Given("^I navigate to Notification Center$")
	public void iNavigateToNotificationCenter() throws Exception {
		if (elementExists(notificationIconBeforeLogin)) {
			loopClickUntilFound(notificationIconBeforeLogin, notificationHeader);
		} else {
			loopClickUntilFound(notificationIconAndroidOptional, notificationHeader);
		}
    	waitFor(3);
    	screenshot(); // Cap screen in notification center
    }

	@Then("^I verify Hospital Authority messages are in correct order$")
	public void iVerifyHAMsgsAreInCorrectOrder() throws Exception {
		String xpath;
		if (isAndroid()) {
			xpath = "xpath: //*[@text='" + TestData.firstHaMessage + "']//following::*[@text='" + TestData.secondHaMessage + "']//following::*[@text='" + TestData.thirdHaMessage + "']";
		} else {
			xpath = "xpath: (((//*[contains(@name, '" + TestData.firstHaMessage + "')])[last()]//following::*[contains(@name, '" + TestData.secondHaMessage + "')])[1]//following::*[contains(@name, '" + TestData.thirdHaMessage + "')])[1]";
		}
		loopSwipeUntilFound(xpath, false);
		waitFor(3);
		screenshot();
	}

	@Then("^I verify \"([^\"]*)\" does not exist$")
	public void iVerifySomeIsGone(String target) throws Exception {
		String targetFromExcel = getValueFromExcel(target);
		String xpath;
		if (isAndroid()) {
			xpath = "xpath: //*[contains(@text, '" + targetFromExcel + "')]";
		} else {
			xpath = "xpath: (//*[contains(@name, '" + targetFromExcel + "')])[last()]";
		}
		checkNonExistence(xpath);
		waitFor(3);
		screenshot();
	}

	@Then("^I verify \"([^\"]*)\" is shown correctly$")
	public void iVerifySomethingIsShown(String target) throws Exception {
		String targetFromExcel = getValueFromExcel(target);
		String errorText = target + " is not shown";
		waitFor(3);
		String xpath;
		String haNotificationHeader = getGeneralMessageHeader();
		switch (targetFromExcel) {
			case "first Hospital Authority notification":
				if (isAndroid()) {
					xpath = "xpath: //*[@text='" + haNotificationHeader + "']//following::*[@text='" + TestData.firstHaMessage + "']";
				} else {
					xpath = "xpath: (//*[contains(@name, '" + haNotificationHeader + " " + TestData.firstHaMessage + "')])[last()]";
				}
				print("before looping swipe first msg");
				loopSwipeUntilFound(xpath, false);
				print("after looping swipe first msg");
				break;
			case "second Hospital Authority notification":
				if (isAndroid()) {
					xpath = "xpath: //*[@text='" + haNotificationHeader + "']//following::*[@text='" + TestData.secondHaMessage + "']";
				} else {
					xpath = "xpath: (//*[contains(@name, '" + haNotificationHeader + " " + TestData.secondHaMessage + "')])[last()]";
				}
				loopSwipeUntilFound(xpath, false);
				break;
			case "third Hospital Authority notification":
				if (isAndroid()) {
					xpath = "xpath: //*[@text='" + haNotificationHeader + "']//following::*[@text='" + TestData.thirdHaMessage + "']";
				} else {
					xpath = "xpath: (//*[contains(@name, '" + haNotificationHeader + " " + TestData.thirdHaMessage + "')])[last()]";
				}
				loopSwipeUntilFound(xpath, false);
				break;
			case Const.myAppointments:
				loopSwipeUntilFound(myAppointmentMiniAppVerification, true);
				break;
			case Const.payha:
				loopSwipeUntilFound(payHaMiniAppVerification, true);
				break;
			case Const.rehab:
				// TODO: An error occurs while entering rehab module, so skip this part first
				break;
			default:
				if (isAndroid()) {
					checkExistence("xpath: //*[contains(@text, '" + targetFromExcel + "')]");
				} else {
					checkExistence("xpath: (//*[contains(@name, '" + targetFromExcel + "')])[last()]");
				}
		}
		waitFor(3);
		screenshot();
	}

	@When("^I click \"([^\"]*)\" \"([^\"]*)\"$")
	public void iClickSomeoneMiniApp(String username, String miniAppName) throws Exception {
		print("Start");
		waitFor(3);
		String xpath;
		String notificationContent;
		String miniAppNameFromExcel = getValueFromExcel(miniAppName);
		String filterValue = getFilterValue(username);
		print("Switch");
		switch (miniAppNameFromExcel) {
			case Const.myAppointments:
				notificationContent = getValueFromExcel(filterValue + ".myAppointmentNotificationContent");
				break;
			case Const.payhaC:
				notificationContent = getValueFromExcel(filterValue + ".payHaNotificationContentOne");
				break;
			case Const.payha:
				notificationContent = getValueFromExcel(filterValue + ".payHaNotificationContentTwo");
				break;
			case Const.rehab:
				notificationContent = getValueFromExcel(filterValue + ".rehabNotificationContent");
				break;
			case Const.telecare:
				notificationContent = getValueFromExcel(filterValue + ".telecareNotificationContent");
				break;
			default:
				throw new Exception("No such mini app");
		}
		print("Clicking " + miniAppNameFromExcel);
		if (isAndroid()) {
			xpath = "xpath: //*[contains(@text, '" + notificationContent + "')]";
		} else {
			xpath = "xpath: (//*[contains(@name, '" + notificationContent + "')])[last()]";
		}
		loopSwipeUntilFound(xpath, true);
		tap(xpath);
		screenshot();
	}

	@Then("^I verify at least \"([^\"]*)\" Hospital Authority messages exist$")
	public void iVerifyAtLeastHAMsgsExist(String numbers) throws Exception {
		waitFor(3);
		if (isAndroid()) {
			loopSwipeUntilFound("xpath: (//*[@text='" + getGeneralMessageHeader() + "'])[" + numbers + "]", false);
		} else {
			switch (numbers) {
				case "3":
					count = 0;
					while (!elementExists("xpath: //*[contains(@name, '" + TestData.firstHaMessage + "')]")) {
						swipeDown();
						count++;
						if (count > 10) {
							throw new Exception("Failed to find first general message");
						}
					}
					while (!elementExists("xpath: //*[contains(@name, '" + TestData.secondHaMessage + "')]")) {
						swipeDown();
						count++;
						if (count > 10) {
							throw new Exception("Failed to find second general message");
						}
					}
					while (!elementExists("xpath: //*[contains(@name, '" + TestData.thirdHaMessage + "')]")) {
						swipeDown();
						count++;
						if (count > 10) {
							throw new Exception("Failed to find third general message");
						}
					}
					break;
				default:
					throw new Exception("There aren't so many messages");
			}
		}
		screenshot();
	}

	@Then("^I go back to home page from \"([^\"]*)\" and wait for \"([^\"]*)\" seconds$")
	public void iGoBackToPreviousPageAndWait(String target, String seconds) throws Exception {
		if (isAndroid()) {
			goBack();
			if(elementExists(rehabBackBtn)) {
				loopClickUntilSelfGone(rehabBackBtn);
			}
		} else {
			switch (target) {
			case "Notification Center":
				loopClickUntilSelfGone(notificationCenterBackBtn);
				break;
			case "My Appointments":
				loopClickUntilSelfGone(myAppointmentsBackBtn);
				break;
			case "Rehab":
				loopClickUntilSelfGone(rehabBackBtn);
				break;
			case "PayHA":
				loopClickUntilSelfGone(payhaBackBtn);
				break;
		 }
		}
		print("Start waiting for " + seconds + " seconds");
		waitFor(Integer.parseInt(seconds));
		print("Finished waiting for " + seconds + " seconds");
		screenshot();
	}

	@Then("^I logout and return to landing page$")
	public void iLogoutAndReturnToLandingPage() throws Exception {
		tap(logoutBtn);
		if (isAndroid()) {
			tap(confirmLogoutBtn);
		} else {
			print("Auto accept yes button");
		}
		checkExistence(login);
		waitFor(3);
		screenshot();
	}

	@Then("^I go back to notification center$")
	public void iGoBackToNotiCenter() throws Exception {
		if (isAndroid() || (isIOS() && !elementExists(notificationCenterBackBtn))) {
			print("before go back");
			if (isAndroid()) {
				goBack();
			} else {
				loopClickUntilFound(backFromContentBtn, notificationCenterBackBtn);
			}
			print("after go back");
			print("start wait for 3 seconds");
			waitFor(3);
			print("after wait for 3 seconds");
			screenshot();
		}
	}

	@Then("^I verify \"([^\"]*)\" is above \"([^\"]*)\"$")
	public void iVerifyContentOneIsAboveContentTwo(String notificationContentOne, String notificationContentTwo) throws Exception {
		String notificationContentOneFromExcel = getValueFromExcel(notificationContentOne);
		String notificationContentTwoFromExcel = getValueFromExcel(notificationContentTwo);
		String xpathOne;
		String xpathTwo;
		int oneY;
		int twoY;
		String haNotificationHeader = getGeneralMessageHeader();
		if (isAndroid()) {
			xpathOne = "xpath: //*[contains(@text, '" + notificationContentOneFromExcel + "')]";
			if (notificationContentTwo.equals(Const.generalMessages)) {
				xpathTwo = "xpath: //*[@text='" + haNotificationHeader + "']//following::*[@text='" + TestData.firstHaMessage + "']";
				loopSwipeUntilFound(xpathTwo, false);
			} else {
				xpathTwo = "xpath: //*[contains(@text, '" + notificationContentTwoFromExcel + "')]";
				loopSwipeUntilFound(xpathTwo, true);
			}
		} else {
			xpathOne = "xpath: (//*[contains(@name, '" + notificationContentOneFromExcel + "')])[last()]";
			if (notificationContentTwo.equals(Const.generalMessages)) {
				xpathTwo = "xpath: (//*[contains(@name, '" + haNotificationHeader + " " + TestData.firstHaMessage + "')])[last()]";
				loopSwipeUntilFound(xpathTwo, false);
			} else {
				xpathTwo = "xpath: (//*[contains(@name, '" + notificationContentTwoFromExcel + "')])[last()]";
				loopSwipeUntilFound(xpathTwo, true);
			}
		}
		twoY = getYLocation(getElement(xpathTwo));
		print("two y: "+ twoY);
		loopSwipeUntilFound(xpathOne, true);
		oneY = getYLocation(getElement(xpathOne));
		print("One y: "+ oneY);
		if (oneY > twoY) {
			oneY = getYLocation(getElement(xpathOne));
			twoY = getYLocation(getElement(xpathTwo));
			print("Retry one y: "+ oneY);
			print("Retry two y: "+ twoY);
			// Have to check once more to avoid false alarm
			if (oneY > twoY) {
				throw new Exception(notificationContentOne + " is not located higher than " + notificationContentTwoFromExcel);
			}
		}
		waitFor(3);
		screenshot();
	}

	@Then("^I should see a notification about \"([^\"]*)\"$")
	public void iSeeNotiOfMiniAppName(String miniAppName) throws Exception {
		String miniAppNameFromExcel = getValueFromExcel(miniAppName);
		String xpath = getFirstXpathInElementNameFormat(miniAppNameFromExcel);
		switch (miniAppNameFromExcel) {
			default:
				checkExistence(xpath);
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I verify Un-read Notification Count is \"([^\"]*)\"$")
	public void iVerifyUnreadNotificationCountis(String count) throws Exception {
			switch(count) {
			case "3":
				checkExistence(notificationCountBadgeThree);
				break;
			case "1":
				checkExistence(notificationCountBadgeOne);
				break;
			case "0":
				checkExistence(notificationCountBadgeZero);
				break;
			  }
		waitFor(3);
		screenshot();
	}
	
	@Then("^I verify \"([^\"]*)\" next to first notification$") 
	public void iVerifyRedBadgeNextToContentOne(String badge) throws Exception {
			switch(badge) {
			case "Red Badge":
		    	checkExistence(notificationRedBadge);
		    	break;
			case "Red Badge disappears":
				checkExistence(notificationRedBadgeDisappears);
				break;
			}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I click on Search Icon and the SearchBar is displayed$") 
	public void iClickOnSearchIconAndTheSearchBarIsDisplayed() throws Exception {
			if(elementExists(searchIcon)) {
		    	tap(searchIcon);
		    	checkExistence(searchBar);
			}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I input Search string \"([^\"]*)\" into SearchBar text field$") 
	public void iInputSearchStringIntoSearchBarTextField(String text) throws Exception {
			if(elementExists(searchBar)) {
		    	sendkeys(searchBar, text, true, true);
			}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I verify \"([^\"]*)\" is shown in Search Results$") 
	public void iVerifyIsShownInSearchResults(String result) throws Exception {
			checkExistence(notificationSearchResults);
		waitFor(3);
		screenshot();
	}
	
	@Then("^I verify \"([^\"]*)\" notification is \"([^\"]*)\"$") 
	public void iVerifyNotificationIs(String result, String icon) throws Exception {
		if(elementExists(notificationSearchResults)) {
			switch (icon) {
			case "Collapsed":
				checkExistence(expandedIcon);
				break;
			
			case "Expanded":
				checkExistence(collapsedIcon);
				break;
			}
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I clear the Search text$") 
	public void iClearTheSearchText() throws Exception {
		if(elementExists(clearSearchString)) {
			tap(clearSearchString);
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I click inside the SearchBar and a dropdown of Filter Options is displayed$") 
	public void iClickInsideTheSearchBarAndADropdownOfFilterOptionsIsDisplayed() throws Exception {
		if(elementExists(searchBar)) {
			tap(searchBar);
			checkExistence(filterDropdownList);
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I click Filter Option \"([^\"]*)\"$") 
	public void iClickFilterOption(String option) throws Exception {
		switch (option) {
		case "Rehab" :
			tap(filterDropdownListRehab);
			hideKeyboard();
			break;
		case "PayHA" :
			tap(filterDropdownListPayHA);
			hideKeyboard();
			break;
		case "Hospital Authority" :
			tap(filterDropdownListHA);
			hideKeyboard();
			break;
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I verify \"([^\"]*)\" notifications is shown in Search Results$") 
	public void iVerifyNotiicationsIsShownInSearchResults(String option) throws Exception {
		switch (option) {
		case "Rehab" :
			if(elementExists(filterRehabLabel)) {
			checkExistence(filterListRehab);
			}
			break;
		case "PayHA" :
			if(elementExists(filterPayHALabel)) {
				checkExistence(filterListPayhaOne);
				checkExistence(filterListPayhaTwo);
				}
			break; 
		case "Hospital Authority" :
			if(elementExists(filterHALabel)) {
			checkExistence(filterListHA);
			}
			break;
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I click on caree user \"([^\"]*)\"$") 
	public void iClickOnCareeUser(String user) throws Exception {
		switch (user) {
			case "LAU, SIU YUEN":
				if(elementExists(undercareUser)) {
					tap(undercareUser);
				  }
				break;
		}
		waitFor(3);
		screenshot();
	}
	
	@Then("^I verify user \"([^\"]*)\" is displayed$") 
	public void iVerifyUserIsDisplayed(String user) throws Exception {
		switch (user) {
			case "LAU, SIU YUEN":
				if (isAndroid()) {
					checkExistence(undercareUser);
					break;
				} else {
					if(elementExists(undercareUserIos)) {
						checkExistence(undercareUserIos);
					  } else {
						 checkExistence(undercareUserIosNC);
					  }
					break;
				}		
		}
		waitFor(3);
		screenshot();
	}

	@Given("^I click \"([^\"]*)\" link$")
	public void iClickSomeLink(String linkName) throws Exception {
		String linkNameFromExcel = getValueFromExcel(linkName);
		String xpath = getFirstXpathInElementNameFormat(linkNameFromExcel);
		switch (linkNameFromExcel) {
			case "詳情":
				if (isAndroid()) {
					xpath = "xpath: //*[contains(@text, '" + linkNameFromExcel + "')]";
				} else {
					xpath = "xpath: (//*[contains(@name, '" + linkNameFromExcel + "')])[last()]";
				}
				Point point = getElement(xpath).getLocation();
				Dimension elementDimension = getElement(xpath).getSize();
				Dimension dimension = getOperation().getDriver().manage().window().getSize();
				int sizeX = dimension.width;
				int sizeY = dimension.height;
				int locationX = point.x;
				int locationY = point.y;
				int elementX = elementDimension.width;
				int elementY = elementDimension.height;
				tapByCoordinator(locationX + 10, locationY + (elementY / 2));
				print("SizeX: " + sizeX);
				print("SizeY: " + sizeY);
				print("locationX: " + locationX);
				print("locationY: " + locationY);
				print("elementX: " + elementX);
				print("elementY: " + elementY);
				break;
			default:
				tap(xpath);
		}
		waitFor(3);
		screenshot();
	}

	@Given("^I click \"([^\"]*)\" notification$")
	public void iClickSomeNotification(String notificationName) throws Exception {
		String notificationNameFromExcel = getValueFromExcel(notificationName);
		String xpath = getFirstXpathInElementNameFormat(notificationNameFromExcel);
		String targetPath;
		switch (notificationNameFromExcel) {
			case "first Hospital Authority":
				if (isAndroid()) {
					targetPath = "xpath: //*[@text='" + TestData.firstHaMessage + "']";
				} else {
					targetPath = "xpath: (//*[contains(@name, '" + TestData.firstHaMessage + "')])[last()]";
				}
				loopSwipeUntilFound(targetPath, false);
				print("before clicking first ha msg");
				tap(targetPath);
				print("after clicking first ha msg");
				break;
			case "second Hospital Authority":
				if (isAndroid()) {
					targetPath = "xpath: //*[@text='" + TestData.secondHaMessage + "']";
				} else {
					targetPath = "xpath: (//*[contains(@name, '" + TestData.secondHaMessage + "')])[last()]";
				}
				loopSwipeUntilFound(targetPath, false);
				tap(targetPath);
				break;
			case "third Hospital Authority":
				if (isAndroid()) {
					targetPath = "xpath: //*[@text='" + TestData.thirdHaMessage + "']";
				} else {
					targetPath = "xpath: (//*[contains(@name, '" + TestData.thirdHaMessage + "')])[last()]";
				}
				loopSwipeUntilFound(targetPath, false);
				tap(targetPath);
				break;
			default:
				tapIfExists(xpath);
		}
		waitFor(3);
		screenshot();
	}

	@Given("^I fill in verification code$")
	public void iFillInVerificationCode() throws Exception {
		if (elementExists(verifyMobileNumberPageHeader)) {
			if (isAndroid()) {
				/// Some android devices only accept keycode flow
				try {
					((AndroidDriver) getOperation().getDriver()).pressKey(new KeyEvent(AndroidKey.DIGIT_1));
					((AndroidDriver) getOperation().getDriver()).pressKey(new KeyEvent(AndroidKey.DIGIT_1));
					((AndroidDriver) getOperation().getDriver()).pressKey(new KeyEvent(AndroidKey.DIGIT_1));
					((AndroidDriver) getOperation().getDriver()).pressKey(new KeyEvent(AndroidKey.DIGIT_1));
					((AndroidDriver) getOperation().getDriver()).pressKey(new KeyEvent(AndroidKey.DIGIT_1));
					((AndroidDriver) getOperation().getDriver()).pressKey(new KeyEvent(AndroidKey.DIGIT_1));
				} catch (Exception e) {
					sendKeys("1");
					sendKeys("1");
					sendKeys("1");
					sendKeys("1");
					sendKeys("1");
					sendKeys("1");
				}
			} else {
				sendKeys("1");
				sendKeys("1");
				sendKeys("1");
				sendKeys("1");
				sendKeys("1");
				sendKeys("1");
			}

			if (elementExists(receivePushBtn)) {
				tap(receivePushBtn);
			}
			
			if (elementExists(switchDeviceBtn)) {
				tap(switchDeviceBtn);
			}
		}
		waitForDisappearance(verifyMobileNumberPageHeader);
		waitFor(3);
		leaveAuthorizeTouchIdPage();
		screenshot();
		
	}

	@Given("^I click \"([^\"]*)\" button$")
	public void iClickSomeButton(String btnName) throws Exception {
		String btnNameFromExcel = getValueFromExcel(btnName);
		String xpath = getFirstXpathInElementNameFormat(btnNameFromExcel);
		switch (btnNameFromExcel) {
			case "Login":
				tap(submitLogin);
				leaveAuthorizeTouchIdPage();
				if (elementExists(switchDeviceBtn)) {
					tap(switchDeviceBtn);
				}
				if (elementExists(multipleDeviceBtn)) {
					tap(multipleDeviceBtn);
				}
				break;
			default:
				tapIfExists(xpath);
		}
		waitFor(3);
		screenshot();
	}

	@Given("^I input \"([^\"]*)\" into \"([^\"]*)\" text field")
	public void iInputSomethingIntoTextField(String inputValue, String textFieldName) throws Exception {
		String inputValueFromExcel = getValueFromExcel(inputValue);
		String textFieldNameFromExcel = getValueFromExcel(textFieldName);
		switch (textFieldNameFromExcel) {
			case "Login username":
				sendkeys(usernameTextField, inputValueFromExcel, true, true);
				break;
			case "Login password":
				sendkeys(passwordTextField, inputValueFromExcel, true, true);
				break;
			default:
				throw new Exception("No such text field: " + textFieldNameFromExcel);
		}
		waitFor(3);
		screenshot();
	}
	
	@Given("^I verify \"([^\"]*)\" is highlighted$")
	public void iverifyIsHighlighted(String highlight) throws Exception {
		    checkExistence(highlightedWord);
		waitFor(3);
		screenshot();
	}
	
	@Given("^I navigate to Undercare User Icon$")
	public void inavigateToUndercareUserIcon() throws Exception {
		if (elementExists(undercareUserIcon)) {
		    tap(undercareUserIcon);
		} else {
			checkExistence(undercareUserIcons);
			tap(undercareUserIcons);
		}
		waitFor(3);
		screenshot();
	}
	
	@Given("^I navigate to caree Notification Center$")
	public void inavigateToCareeNotificationCenter() throws Exception {
		if (elementExists(undercareUserBellIcon)) {
		    tap(undercareUserBellIcon);
		}
		waitFor(3);
		screenshot();
	}
	
	@Given("^I verify notification center bell icon has \"([^\"]*)\" indicator$") 
	public void iVerifyNotificationCenterBellIconHasIndicator(String indicator) throws Exception {
		switch (indicator) {
			case "Message":
				checkExistence(notificationIconIndicator);
				break;
			case "No":
				checkNonExistence(notificationIconIndicator);
				break;
		}	
		waitFor(3);
		screenshot();
	}
}
