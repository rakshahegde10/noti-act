################################################################
#HAGo: More setting - begins
################################################################
hagoC.skip = xpath: //*[@text='略過']
hagoE.skip = xpath: //*[@text='Skip']
hagoE.jailbreakConfirm = xpath: //*[@text='CONFIRM']
hagoC.jailbreakConfirm = xpath: //*[@text='確定']
hagoC.newVersionCancel = xpath: //*[@text='取消']
hagoE.newVersionCancel = xpath: //*[@text='CANCEL']
hago.virusNotification = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView
hago.strFreqUsedApp = xpath: //*[@text='Frequently Used']
hago.bookha = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ImageView
hago.bookhaAfterLogin = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView
hago.btnBookhaBackSymbol = aid: Navigate up
hago.moreC = xpath: //*[@text='更多']
hago.moreLanguageOptionC = xpath: //*[@text='語言']
hago.moreLanguageOptionSelectEnglishC = xpath: //*[@text='English']
hago.homeE = xpath: //*[@text='Home']
hago.moreE = xpath: //*[@text='More']
hago.moreLanguageOptionE = xpath: //*[@text='Language']
hago.moreLanguageOptionSelectChineseE = xpath: //*[@text='繁體中文']

hagoE.more = xpath: //*[@text='More']
hagoC.more = xpath: //*[@text='更多']
hagoE.smallText = xpath: //*[@text='Small']
hagoC.smallText = xpath: //*[@text='小']
hagoE.mediumText = xpath: //*[@text='Medium']
hagoC.mediumText = xpath: //*[@text='中']
hagoE.bigText = xpath: //*[@text='Large']
hagoC.bigText = xpath: //*[@text='大']
hagoE.fontSizeOption = xpath: //*[@text='Font Size']
hagoC.fontSizeOption = xpath: //*[@text='字體大小']
hagoC.home = xpath: //*[@text='首頁']
hagoE.home = xpath: //*[@text='Home']

hago.homeC = xpath: //*[@text='首頁']
hagoE.popup.combatEpidemic.btn.ok = xpath: //*[@text='OK']
hagoC.popup.combatEpidemic.btn.ok = xpath: //*[@text='確定']
hagoE.popup.weather.btn.ok = xpath: //*[@text='OK']
hagoC.popup.weather.btn.ok = xpath: //*[@text='確定']
hagoE.popup.multiple.device.btn.ok = xpath: //*[@text='OK']
hagoC.popup.multiple.device.btn.ok = xpath: //*[@text='確定']

hagoE.popup.receivepush.btn.confirm = xpath: //*[@text='YES']
hagoC.popup.receivepush.btn.confirm = xpath: //*[@text='是']

hagoE.popup.switchDevice.btn.confirm = xpath: //*[@text='SWITCH']
hagoC.popup.switchDevice.btn.confirm = xpath: //*[@text='轉用']

hagoE.tap.rehab.popup = xpath: //*[@text='OK']
hagoC.tap.rehab.popup = xpath: //*[@text='好']	

################################################################
#HAGo: Notification Cases
################################################################
hagoE.logout.btn = xpath: (//*[contains(@text, 'Logout')])[1]
hagoC.logout.btn = xpath: (//*[contains(@text, '登出')])[1]

hagoE.notification.page.back.btn = xpath: //*[contains(@text, 'My Notifications')]
hagoC.notification.page.back.btn = xpath: //*[contains(@text, 'My Notifications')]

hagoE.myappointments.page.back.btn = xpath: //android.widget.ImageView
hagoC.myappointments.page.back.btn = xpath: //android.widget.ImageView

hagoE.payha.page.back.btn = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView
hagoC.payha.page.back.btn = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView

hagoE.logout.btn.confirm = xpath: //*[@text='YES']
hagoC.logout.btn.confirm = xpath: //*[@text='是']

hagoE.payha.content = xpath: //*[contains(@text, "Today's Fees")]
hagoC.payha.content = xpath: //*[contains(@text, '今天費用')]

hagoE.my.appointments.content = xpath: //*[contains(@text, '* HA Go will display other appointments by phases (Details)')]
hagoC.my.appointments.content = xpath: //*[contains(@text, '* HA Go會分階段顯示其他覆診期（詳情')]

hagoE.main.page.notification.icon.before.login = xpath: //android.widget.TextView[@text='Register']//following::android.widget.ImageView
hagoC.main.page.notification.icon.before.login = xpath: //android.widget.TextView[@text='帳戶註冊']//following::android.widget.ImageView

hago.main.page.notification.icon.before.login.android.optional = xpath: (//android.widget.ImageView)[3]

hagoC.main.page.notification.icon.basic.member = xpath: (//android.view.ViewGroup[4])[1]
hagoE.main.page.notification.icon.basic.member = xpath: (//android.view.ViewGroup[4])[1]

hagoE.main.page.notification.icon.after.login = xpath: (//android.widget.ImageView[1])[4]
hagoC.main.page.notification.icon.after.login = xpath: (//android.widget.ImageView[1])[4]

hago.main.page.notification.icon.after.login.android.optional = xpath: (//android.view.ViewGroup[3])[1]

hagoE.main.page.activation.page.leave.btn = xpath: //*[@text='Go to clinic later']
hagoC.main.page.activation.page.leave.btn = xpath: //*[@text='稍後前往診所']

hagoE.main.page.touchid.negative.btn = xpath: //*[@text='NOT NOW']
hagoC.main.page.touchid.negative.btn = xpath: //*[@text="稍後"]

hagoE.main.page.faceid.header = xpath: (//XCUIElementTypeOther[@name="Authorise Face ID"])[3]
hagoC.main.page.faceid.header = xpath: (//XCUIElementTypeOther[@name="授權Face ID"])[3]

hagoE.main.page.touchid.ok.btn = xpath: //*[@text="OK"]
hagoC.main.page.touchid.ok.btn = xpath: //*[@text="確定"]

hagoE.main.page.touchid.header = xpath: //*[@text="Authorise Touch ID"]
hagoC.main.page.touchid.header = xpath: //*[@text="授權Touch ID"]

hagoE.authorise.touchid.page.back.btn = xpath: (//XCUIElementTypeOther[@name="Authorise Touch ID"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.authorise.touchid.page.back.btn = xpath: (//XCUIElementTypeOther[@name="授權Touch ID"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.verify.mobile.number.header = xpath: (//*[@text="Verify Your Mobile Number"])[1]
hagoC.verify.mobile.number.header = xpath: (//*[@text="驗證流動電話號碼"])[1]

hagoE.notification.page.header = xpath: //*[@text='Notification Centre']
hagoC.notification.page.header = xpath: //*[@text='訊息中心']

hagoE.unread.notification.count.badge.one = xpath: //android.widget.TextView[@text="1"]
hagoC.unread.notification.count.badge.one = xpath: //android.widget.TextView[@text="1"]

hagoE.unread.notification.count.badge.zero = xpath: //android.widget.TextView[@text="0"]
hagoC.unread.notification.count.badge.zero = xpath: //android.widget.TextView[@text="0"]

hagoE.unread.notification.count.badge.three = xpath: //android.widget.TextView[@text="3"]
hagoC.unread.notification.count.badge.three = xpath: //android.widget.TextView[@text="3"]

hagoE.unread.notification.red.dot = xpath: //android.view.ViewGroup[2]
hagoC.unread.notification.red.dot = xpath: //android.view.ViewGroup[2]

hagoE.unread.notification.red.dot.disappears = xpath: (//android.view.ViewGroup[2])[2]
hagoC.unread.notification.red.dot.disappears = xpath: (//android.view.ViewGroup[2])[2]

hagoE.click.search.icon = xpath: //android.widget.TextView[@text=""]
hagoC.click.search.icon = xpath: //android.widget.TextView[@text=""]

hagoE.display.search.bar = xpath: //android.widget.EditText[@text="Search Here..."]
hagoC.display.search.bar = xpath: //android.widget.EditText[@text="Search Here..."]

hagoE.display.search.bar.outpatient = xpath: //android.widget.TextView[@text="outpatient"]
hagoC.display.search.bar.outpatient = xpath: //android.widget.TextView[@text="outpatient"]

hagoE.verify.notification.search.bar = xpath: (//android.widget.TextView[contains(@text, "Since the Hong Kong Observatory")])
hagoC.verify.notification.search.bar = xpath: (//android.widget.TextView[contains(@text, "Since the Hong Kong Observatory")])

hagoE.expanded.icon = xpath: //android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView
hagoC.expanded.icon = xpath: //android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView

hagoE.collapsed.icon = xpath: //android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView
hagoC.collapsed.icon = xpath: //android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView

hagoE.highlighted.word = xpath: (//android.widget.TextView[contains(@text, "outpatient")])
hagoC.highlighted.word = xpath: (//android.widget.TextView[contains(@text, "outpatient")])

hagoE.clear.search.string = xpath: //android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView
hagoC.clear.search.string = xpath: //android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView

hagoE.filter.dropdown.list = xpath: //android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup
hagoC.filter.dropdown.list = xpath: //android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup

hagoE.filter.dropdown.list.rehab = xpath: //android.widget.TextView[@text="Rehab"]
hagoC.filter.dropdown.list.rehab = xpath: //android.widget.TextView[@text="Rehab"]

hagoE.filter.rehab.message = xpath: (//android.widget.TextView[contains (@text, "您有一個康復提示定於下午05:37")])
hagoC.filter.rehab.message = xpath: (//android.widget.TextView[contains (@text, "您有一個康復提示定於下午05:37")])

hagoE.filter.rehab.label = xpath: (//android.widget.TextView[@text="Rehab"])[1]
hagoC.filter.rehab.label = xpath: (//android.widget.TextView[@text="Rehab"])[1]

hagoE.filter.dropdown.list.payha = xpath: //android.widget.TextView[@text="Pay HA"]
hagoC.filter.dropdown.list.payha = xpath: //android.widget.TextView[@text="Pay HA"]

hagoE.filter.payha.message1 = xpath: (//android.widget.TextView[contains (@text, "Bill #BTKO2000031860U")])
hagoC.filter.payha.message1 = xpath: (//android.widget.TextView[contains (@text, "Bill #BTKO2000031860U")])

hagoE.filter.payha.message2 = xpath: (//android.widget.TextView[contains (@text, "門診編號#SOPD1910837Z")])
hagoC.filter.payha.message2 = xpath: (//android.widget.TextView[contains (@text, "門診編號#SOPD1910837Z")])

hagoE.filter.payha.label = xpath: (//android.widget.TextView[@text="Pay HA"])[1]
hagoC.filter.payha.label = xpath: (//android.widget.TextView[@text="Pay HA"])[1]

hagoE.filter.dropdown.list.ha = xpath: //android.widget.TextView[@text="Hospital Authority"]
hagoC.filter.dropdown.list.ha = xpath: //android.widget.TextView[@text="Hospital Authority"]

hagoE.filter.ha.label = xpath: (//android.widget.TextView[@text="Hospital Authority"])[1]
hagoC.filter.ha.label = xpath: (//android.widget.TextView[@text="Hospital Authority"])[1]

hagoE.filter.ha.message = xpath: //android.widget.TextView[contains (@text, "HA General Message")]
hagoC.filter.ha.message = xpath: //android.widget.TextView[contains (@text, "HA General Message")]

hagoE.undercare.user.icon = xpath: (//android.widget.ImageView[1])[1]
hagoC.undercare.user.icon = xpath: (//android.widget.ImageView[1])[1]

hagoE.undercare.user = xpath: (//android.widget.TextView[@text="LAU, SIU YUEN"])
hagoC.undercare.user = xpath: (//android.widget.TextView[@text="LAU, SIU YUEN"])

hagoE.undercare.user.notification.center.icon = xpath: (//android.widget.ImageView[1])[4]
hagoC.undercare.user.notification.center.icon = xpath: (//android.widget.ImageView[1])[4]

hagoE.notification.icon.indicator = xpath: (//android.view.ViewGroup)[1]
hagoC.notification.icon.indicator = xpath: (//android.view.ViewGroup)[1]

################################################################
#BookHA: English Version - Enquiry - begins
################################################################
mBookingE.search = id: hk.org.ha.testhago:id/btnEnquiry
mBookingE.declaration = id: hk.org.ha.mbooking:id/cbDeclaration
mBookingE.confirm = id: hk.org.ha.mbooking:id/ibtnConfirm
mBookingE.continue = id: android:id/button1
mBookingE.applicationStatus = id: hk.org.ha.testhago:id/ibtnPatientEnquiry
mBookingE.mb = id: hk.org.ha.testhago:id/etReferenceNumber 
mBookingE.hkid1st = id: hk.org.ha.testhago:id/etHkidCharText
mBookingE.hkid2nd = id: hk.org.ha.testhago:id/etHkidDigitText
mBookingE.hkid3rd = id: hk.org.ha.testhago:id/etHkidCheckDigitText
mBookingE.done = aid: Done
mBookingE.next = id: hk.org.ha.testhago:id/ibtnEnquirySubmit
mBookingE.confirmTestEnv = id: android:id/button1
mBookingE.numPad1 = aid: 1
mBookingE.numPad2 = aid: 2
mBookingE.numPad3 = aid: 3
mBookingE.numPad4 = aid: 4
mBookingE.numPad5 = aid: 5
mBookingE.numPad6 = aid: 6
mBookingE.numPad7 = aid: 7
mBookingE.numPad8 = aid: 8
mBookingE.numPad9 = aid: 9
mBookingE.numPad0 = aid: 0
mBookingE.numPadA = aid: A
mBookingE.back = xpath: (//XCUIElementTypeButton[@name="Back"])[2]
mBookingE.myAppBack= coordinator: 20,45
mBookingE.enquiryBack = coordinator: 20,45

################################################################
#BookHA: English Version - Submit Application - begins
################################################################
mBookingE.submitApplication = xpath: //android.widget.ImageButton[@content-desc="Submit Request"]
mBookingE.saReceivedHealthcareYes = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[1]
mBookingE.saReceivedHealthcareConfirm= id: android:id/button2
mBookingE.saApplicationNoteContinue = id: android:id/button2
mBookingE.saTestEnvConfirm = id: android:id/button1
mBookingE.saID1stHKID = id: hk.org.ha.testhago:id/etHkidCharText
mBookingE.saID2ndHKID = id: hk.org.ha.testhago:id/etHkidDigitText
mBookingE.saID3rdHKID = id: hk.org.ha.testhago:id/etHkidCheckDigitText
mBookingE.saIDSurname = id: hk.org.ha.testhago:id/etSurname
mBookingE.saIDGivenname = id: hk.org.ha.testhago:id/etFirstName
mBookingE.saIDMobileNo = id: hk.org.ha.testhago:id/etMobilePhoneInput
mBookingE.saIDSpecialty= id: android:id/text1
mBookingE.saIDSpecialtyValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[5]
mBookingE.saIDHospital = id: hk.org.ha.testhago:id/spnPreferredClinic
mBookingE.saIDHospitalValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[6]
mBookingE.saIDDone = id: //XCUIElementTypeButton[@name="Done"]
mBookingE.saIDNext = id: hk.org.ha.testhago:id/btnInputFormSubmit
mBookingE.saSDPhotoTaking = id: hk.org.ha.testhago:id/ibtn_addressProofCamera
mBookingE.saGeneralPhotoCapture = xpath: //XCUIElementTypeButton[@name="PhotoCapture"] 542-1796
mBookingE.saGeneralUsePhoto = id: hk.org.ha.testhago:id/preview_ok_btn
mBookingE.saSDNext = id: hk.org.ha.testhago:id/ibtn_hkidSubmit
mBookingE.srlOtherDoctorReferral = id: hk.org.ha.testhago:id/rb_camera
mBookingE.srlPhotoTaking1 = id: hk.org.ha.testhago:id/iv_referralLetter1
mBookingE.srlPhotoTaking2 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]
mBookingE.srlPhotoTaking3 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
mBookingE.srlPhotoTaking4 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]
mBookingE.srlPhotoTakingButton = coordinator: 542,1796
mBookingE.srlNext = id: hk.org.ha.testhago:id/ibtn_referralSubmit
mBookingE.checkNext = id: hk.org.ha.testhago:id/ibtnVerifySubmit
mBookingE.saGeneralFinish = id: hk.org.ha.testhago:id/ibtnBackHome

################################################################
#HAGo: English Version - Edit Profile - begins
################################################################
hagoE.login = xpath: //*[@text='Login']
hagoE.search = aid: Enquiry
hagoE.username = xpath: //android.widget.EditText[1]
hagoE.password = xpath: //android.widget.EditText[2]
hagoE.login2 = xpath: (//*[contains(@text, 'Login')])[2]
hagoE.useTouchIDNo = id: android:id/button2
hagoE.touchIDEnableLaterMsg = id: android:id/button1
hagoE.logout = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView
hagoE.logoutConfirmYes = id: android:id/button1
hagoE.profile = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView
hagoE.emailEdit = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView
hagoE.emailEditPassword = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText
hagoE.emailEditConfirm = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]
hagoE.emailEditNewEmail = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText
hagoE.emailEditSave = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView
hagoE.emailEditConfirmLater = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]
hagoE.emailEditChangeNotEffective = id: android:id/button1
hagoE.profileBackSymbol = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView
hagoE.mobileEdit = xpath: (//XCUIElementTypeOther[@name="Edit"])[4]

################################################################
#HAGo: English Version - My Appointment - begins
################################################################
hagoE.appMyappt = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView
hagoE.myapptTabFuture = xpath: //android.view.ViewGroup[@content-desc="FUTURETITLE"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView
hagoE.myapptTabPast = xpath: //android.view.ViewGroup[@content-desc="PASTTITLE"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView
hagoE.myapptBtnFutureShowCancelRescheduleRecord = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.Switch
hagoE.myapptBtnPastShowCancelRescheduleRecord = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.Switch
hagoE.myapptBtnMyapptBackSymbol = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView

################################################################
#BookHA: Chinese Version - Enquiry - begins
################################################################
mBookingC.search = id: hk.org.ha.testhago:id/btnEnquiry
mBookingC.declaration = id: hk.org.ha.mbooking:id/cbDeclaration
mBookingC.confirm = id: hk.org.ha.mbooking:id/ibtnConfirm
mBookingC.continue = id: android:id/button1
mBookingC.applicationStatus = id: hk.org.ha.testhago:id/ibtnPatientEnquiry
mBookingC.mb = id: hk.org.ha.testhago:id/etReferenceNumber 
mBookingC.hkid1st = id: hk.org.ha.testhago:id/etHkidCharText
mBookingC.hkid2nd = id: hk.org.ha.testhago:id/etHkidDigitText
mBookingC.hkid3rd = id: hk.org.ha.testhago:id/etHkidCheckDigitText
mBookingC.done = aid: Done
mBookingC.next = id: hk.org.ha.testhago:id/ibtnEnquirySubmit
mBookingC.confirmTestEnv = id: android:id/button1
mBookingC.numPad1 = aid: 1
mBookingC.numPad2 = aid: 2
mBookingC.numPad3 = aid: 3
mBookingC.numPad4 = aid: 4
mBookingC.numPad5 = aid: 5
mBookingC.numPad6 = aid: 6
mBookingC.numPad7 = aid: 7
mBookingC.numPad8 = aid: 8
mBookingC.numPad9 = aid: 9
mBookingC.numPad0 = aid: 0
mBookingC.numPadA = aid: A
mBookingC.back = xpath: (//XCUIElementTypeButton[@name="Back"])[2]
mBookingC.myAppBack= coordinator: 20,45
mBookingC.enquiryBack = coordinator: 20,45

################################################################
#BookHA: Chinese Version - Submit Application - begins
################################################################
mBookingC.submitApplication = xpath: //android.widget.ImageButton[@content-desc="Submit Request"]
mBookingC.saReceivedHealthcareYes = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[1]
mBookingC.saReceivedHealthcareConfirm= id: android:id/button2
mBookingC.saApplicationNoteContinue = id: android:id/button2
mBookingC.saTestEnvConfirm = id: android:id/button1
mBookingC.saID1stHKID = id: hk.org.ha.testhago:id/etHkidCharText
mBookingC.saID2ndHKID = id: hk.org.ha.testhago:id/etHkidDigitText
mBookingC.saID3rdHKID = id: hk.org.ha.testhago:id/etHkidCheckDigitText
mBookingC.saIDSurname = id: hk.org.ha.testhago:id/etSurname
mBookingC.saIDGivenname = id: hk.org.ha.testhago:id/etFirstName
mBookingC.saIDMobileNo = id: hk.org.ha.testhago:id/etMobilePhoneInput
mBookingC.saIDSpecialty= id: android:id/text1
mBookingC.saIDSpecialtyValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[5]
mBookingC.saIDHospital = id: hk.org.ha.testhago:id/spnPreferredClinic
mBookingC.saIDHospitalValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[6]
mBookingC.saIDDone = id: //XCUIElementTypeButton[@name="Done"]
mBookingC.saIDNext = id: hk.org.ha.testhago:id/btnInputFormSubmit
mBookingC.saSDPhotoTaking = id: hk.org.ha.testhago:id/ibtn_addressProofCamera
mBookingC.saGeneralPhotoCapture = xpath: //XCUIElementTypeButton[@name="PhotoCapture"] 542-1796
mBookingC.saGeneralUsePhoto = id: hk.org.ha.testhago:id/preview_ok_btn
mBookingC.saSDNext = id: hk.org.ha.testhago:id/ibtn_hkidSubmit
mBookingC.srlOtherDoctorReferral = id: hk.org.ha.testhago:id/rb_camera
mBookingC.srlPhotoTaking1 = id: hk.org.ha.testhago:id/iv_referralLetter1
mBookingC.srlPhotoTaking2 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]
mBookingC.srlPhotoTaking3 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
mBookingC.srlPhotoTaking4 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]
mBookingC.srlPhotoTakingButton = coordinator: 542,1796
mBookingC.srlNext = id: hk.org.ha.testhago:id/ibtn_referralSubmit
mBookingC.checkNext = id: hk.org.ha.testhago:id/ibtnVerifySubmit
mBookingC.saGeneralFinish = id: hk.org.ha.testhago:id/ibtnBackHome

################################################################
#HAGo: Chinese Version - Edit Profile - begins
################################################################
hagoC.login = xpath: //*[@text='登入']
hagoC.search = aid: Enquiry	
hagoC.username = xpath: //android.widget.EditText[1]
hagoC.password = xpath: //android.widget.EditText[2]
hagoC.login2 = xpath: (//*[contains(@text, '登入')])[2]
hagoC.useTouchIDNo = id: android:id/button2
hagoC.touchIDEnableLaterMsg = id: android:id/button1
hagoC.logout = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView
hagoC.logoutConfirmYes = id: android:id/button1
hagoC.profile = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView
hagoC.emailEdit = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView
hagoC.emailEditPassword = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText
hagoC.emailEditConfirm = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]
hagoC.emailEditNewEmail = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText
hagoC.emailEditSave = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView
hagoC.emailEditConfirmLater = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]
hagoC.emailEditChangeNotEffective = id: android:id/button1
hagoC.profileBackSymbol = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView
hagoC.mobileEdit = xpath: (//XCUIElementTypeOther[@name="Edit"])[4]

################################################################
#HAGo: Chinese Version - My Appointment - begins
################################################################
hagoC.appMyappt = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView
hagoC.myapptTabFuture = xpath: //android.view.ViewGroup[@content-desc="FUTURETITLE"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView
hagoC.myapptTabPast = xpath: //android.view.ViewGroup[@content-desc="PASTTITLE"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView
hagoC.myapptBtnFutureShowCancelRescheduleRecord = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.Switch
hagoC.myapptBtnPastShowCancelRescheduleRecord = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.Switch
hagoC.myapptBtnMyapptBackSymbol = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView

################################################################
#HAGo: Pay HA
################################################################
hagoE.payha.icon = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView
hagoE.payha.view = aid: ViewBtn
hagoE.payha.total = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[3]
