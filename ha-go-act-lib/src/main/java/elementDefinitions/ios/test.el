################################################################
#HAGo: More setting - begins
################################################################
hagoC.skip = xpath: (//*[@label='略過'])[2]
hagoE.skip = xpath: (//*[@label='Skip'])[2]
hagoE.jailbreakConfirm = xpath: //*[@name='CONFIRM']
hagoC.jailbreakConfirm = xpath: (//*[@name='確定'])[1]
hagoC.newVersionCancel = xpath: //*[@name='取消']
hagoE.newVersionCancel = xpath: //*[@name='CANCEL']
hago.virusNotification = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView
hago.strFreqUsedApp = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[1]
hago.bookha = coordinator: 192,289
hago.bookhaAfterLogin = xpath: /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.ImageView
hago.btnBookhaBackSymbol = aid: Navigate up

hagoE.more = xpath: //XCUIElementTypeOther[@name="More"]
hagoC.more = xpath: //XCUIElementTypeOther[@name="更多"]
hagoE.smallText = xpath: //XCUIElementTypeOther[@name="Small"]
hagoC.smallText = xpath: //XCUIElementTypeOther[@name="小"]
hagoE.bigText = xpath: //XCUIElementTypeOther[@name="Large"]
hagoC.bigText = xpath: //XCUIElementTypeOther[@name="大"]
hagoE.mediumText = xpath: //XCUIElementTypeOther[@name="Medium"]
hagoC.mediumText = xpath: //XCUIElementTypeOther[@name="中"]
hagoE.fontSizeOption = xpath: (//XCUIElementTypeOther[contains(@name, 'Font Size')])[last()]
hagoC.fontSizeOption = xpath: (//XCUIElementTypeOther[contains(@name, '字體大小')])[last()]
hagoE.home = xpath: //XCUIElementTypeOther[@name="Home"]
hagoC.home = xpath: //XCUIElementTypeOther[@name="首頁"]

hago.moreC= xpath: //XCUIElementTypeOther[@name="更多"]
hago.moreLanguageOptionC = xpath: (//XCUIElementTypeOther[contains(@name, '語言')])[last()]
hago.moreLanguageOptionSelectEnglishC = aid: English
hago.homeE = xpath: //XCUIElementTypeOther[@name="Home"]
hago.moreE = xpath: //XCUIElementTypeOther[@name="More"]
hago.moreLanguageOptionE = xpath: (//XCUIElementTypeOther[contains(@name, 'Language')])[last()]
hago.moreLanguageOptionSelectChineseE = aid: 繁體中文
hago.homeC = xpath: //XCUIElementTypeOther[@name="首頁"]
hagoE.popup.combatEpidemic.btn.ok = xpath: (//*[@label='OK'])[1]
hagoC.popup.combatEpidemic.btn.ok = xpath: (//*[@label='確定'])[1]
hagoE.popup.weather.btn.ok = xpath: (//*[@label='OK'])[1]
hagoC.popup.weather.btn.ok = xpath: (//*[@label='確定'])[1]
hagoE.popup.multiple.device.btn.ok = xpath: (//*[@label='OK'])[1]
hagoC.popup.multiple.device.btn.ok = xpath: (//*[@label='確定'])[1]

hagoE.popup.receivepush.btn.confirm = xpath: (//*[@text='YES'])[1]
hagoC.popup.receivepush.btn.confirm = xpath: (//*[@text='是'])[1]

hagoE.popup.switchDevice.btn.confirm = xpath: (//*[@text='SWITCH'])[1]
hagoC.popup.switchDevice.btn.confirm = xpath: (//*[@text='轉用'])[1]

hagoE.tap.rehab.popup = xpath: (//XCUIElementTypeButton[@name="Ok"])
hagoC.tap.rehab.popup = xpath: (//XCUIElementTypeButton[@name="好"])

################################################################
#HAGo: Notification Cases
################################################################
hagoC.back.from.notification.btn = xpath: (//XCUIElementTypeOther[@name="訊息中心"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoE.back.from.notification.btn = xpath: (//XCUIElementTypeOther[@name="Notification Centre"])[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.logout.btn = xpath: (//XCUIElementTypeOther[@name="Logout "])[2]
hagoC.logout.btn = xpath: (//XCUIElementTypeOther[@name="登出 "])[2]

hagoC.logout.btn.confirm = xpath: //XCUIElementTypeButton[@name="是"]
hagoE.logout.btn.confirm = xpath: //XCUIElementTypeButton[@name="Yes"]

hagoE.payha.content = xpath: //*[contains(@name, "Today's Fees")]
hagoC.payha.content = xpath: //*[contains(@name, "今天費用")]

hagoE.notification.page.back.btn = xpath: (//XCUIElementTypeOther[@name="Notification Centre"])[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.notification.page.back.btn = xpath: (//XCUIElementTypeOther[@name="訊息中心"])[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.myappointments.page.back.btn = xpath: (//XCUIElementTypeOther[@name="My Appointments"])[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.myappointments.page.back.btn = xpath: (//XCUIElementTypeOther[@name="預約紀錄"])[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.my.appointments.content = aid: * HA Go will display other appointments by phases (Details)
hagoC.my.appointments.content = aid: * HA Go會分階段顯示其他覆診期（詳情

hagoE.main.page.notification.icon.before.login = xpath: (//XCUIElementTypeOther[@name="Login Register"])[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther
hagoC.main.page.notification.icon.before.login = xpath: (//XCUIElementTypeOther[@name="登入 帳戶註冊"])[2]/XCUIElementTypeOther[3]/XCUIElementTypeOther

hagoE.main.page.notification.icon.basic.member = xpath: (//XCUIElementTypeOther[contains(@name, 'Hello') and contains(@name, 'Activate your account')])[last()]/XCUIElementTypeOther[4]/XCUIElementTypeOther
hagoC.main.page.notification.icon.basic.member = xpath: (//XCUIElementTypeOther[@name="你好 啟動你的帳戶"])[2]/XCUIElementTypeOther[4]/XCUIElementTypeOther

hagoE.main.page.notification.icon.after.login = xpath: (//XCUIElementTypeOther[contains(@name, 'Hello')])[last()]/XCUIElementTypeOther[3]/XCUIElementTypeOther
hagoC.main.page.notification.icon.after.login = xpath: (//XCUIElementTypeOther[contains(@name, "你好")])[last()]/XCUIElementTypeOther[3]/XCUIElementTypeOther

hagoE.main.page.activation.page.leave.btn = xpath: //*[@name='Go to clinic later']
hagoC.main.page.activation.page.leave.btn = xpath: //*[@name='稍後前往診所']

hagoE.main.page.touchid.negative.btn = xpath: //XCUIElementTypeButton[@name='Not now']
hagoC.main.page.touchid.negative.btn = xpath: //XCUIElementTypeButton[@name="稍後"]

hagoE.main.page.touchid.ok.btn = xpath: //XCUIElementTypeButton[@name="OK"]
hagoC.main.page.touchid.ok.btn = xpath: //XCUIElementTypeButton[@name="確定"]

hagoE.main.page.touchid.header = xpath: (//XCUIElementTypeOther[@name="Authorise Touch ID"])[3]
hagoC.main.page.touchid.header = xpath: (//XCUIElementTypeOther[@name="授權Touch ID"])[3]

hagoE.main.page.faceid.header = xpath: (//XCUIElementTypeOther[@name="Authorise Face ID"])[3]
hagoC.main.page.faceid.header = xpath: (//XCUIElementTypeOther[@name="授權Face ID"])[3]

hagoE.authorise.faceid.page.back.btn.ios.optional = xpath: (//XCUIElementTypeOther[@name="Authorise Face ID"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.authorise.faceid.page.back.btn.ios.optional = xpath: (//XCUIElementTypeOther[@name="授權Face ID"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.authorise.touchid.page.back.btn.ios.optional = xpath: (//XCUIElementTypeOther[@name="Authorise Touch ID"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.authorise.touchid.page.back.btn.ios.optional = xpath: (//XCUIElementTypeOther[@name="授權Touch ID"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.verify.mobile.number.header = xpath: (//XCUIElementTypeOther[@name="Verify Your Mobile Number"])[3]
hagoC.verify.mobile.number.header = xpath: (//XCUIElementTypeOther[@name="驗證流動電話號碼"])[3]

hagoE.notification.page.header = xpath: //*[@label='Notification Centre']
hagoC.notification.page.header = xpath: //*[@label='訊息中心']

hagoE.unread.notification.count.badge.one = xpath: //XCUIElementTypeStaticText[@name="1"]
hagoC.unread.notification.count.badge.one = xpath: //XCUIElementTypeStaticText[@name="1"]

hagoE.unread.notification.count.badge.zero = xpath: //XCUIElementTypeStaticText[@name="0"]
hagoC.unread.notification.count.badge.zero = xpath: //XCUIElementTypeStaticText[@name="0"]

hagoE.unread.notification.count.badge.three = xpath: //XCUIElementTypeStaticText[@name="3"]
hagoC.unread.notification.count.badge.three = xpath: //XCUIElementTypeStaticText[@name="3"]

hagoE.unread.notification.red.dot = xpath: (//XCUIElementTypeOther[contains(@name, '您有一個醫護指引。')])[3]
hagoC.unread.notification.red.dot = xpath: (//XCUIElementTypeOther[contains(@name, '您有一個醫護指引。')])[3]

hagoE.unread.notification.red.dot.disappears = xpath: (//XCUIElementTypeOther[contains(@name, '您有一個醫護指引。')])[3]
hagoC.unread.notification.red.dot.disappears = xpath: (//XCUIElementTypeOther[contains(@name, '您有一個醫護指引。')])[3]

hagoE.click.search.icon = xpath: (//XCUIElementTypeOther[@name=""])[3]
hagoC.click.search.icon = xpath: (//XCUIElementTypeOther[@name=""])[3]

hagoE.display.search.bar = xpath: //XCUIElementTypeOther[@name="Search Here..."]
hagoC.display.search.bar = xpath: //XCUIElementTypeOther[@name="Search Here..."]

hagoE.display.search.bar.outpatient = xpath: //XCUIElementTypeTextField[@value="outpatient"]
hagoC.display.search.bar.outpatient = xpath: //XCUIElementTypeTextField[@value="outpatient"]

hagoE.verify.notification.search.bar = xpath: (//XCUIElementTypeOther[contains(@name, 'Since the Hong Kong Observatory')])[4]
hagoC.verify.notification.search.bar = xpath: (//XCUIElementTypeOther[contains(@name, 'Since the Hong Kong Observatory')])[4]

hagoE.expanded.icon = xpath: //XCUIElementTypeOther[contains(@name, "")])[3]
hagoC.expanded.icon = xpath: //XCUIElementTypeOther[contains(@name, "")])[3]

hagoE.collapsed.icon = xpath: //XCUIElementTypeOther[contains(@name, "")])[4]
hagoC.collapsed.icon = xpath: //XCUIElementTypeOther[contains(@name, "")])[4]

hagoE.highlighted.word = xpath: (//XCUIElementTypeOther[contains(@name, 'outpatient')])[4]
hagoC.highlighted.word = xpath: (//XCUIElementTypeOther[contains(@name, 'outpatient')])[4]

hagoE.clear.search.string = xpath: (//XCUIElementTypeOther[@name=""])[2]
hagoC.clear.search.string = xpath: (//XCUIElementTypeOther[@name=""])[2]

hagoE.filter.dropdown.list = xpath: (//XCUIElementTypeOther[contains(@name, 'Pay HA') and contains(@name, 'Rehab') and contains(@name, 'My Appointments') and contains(@name, 'Hospital Authority')])[2]
hagoC.filter.dropdown.list = xpath: (//XCUIElementTypeOther[contains(@name, 'Pay HA') and contains(@name, 'Rehab') and contains(@name, 'My Appointments') and contains(@name, 'Hospital Authority')])[2]

hagoE.filter.dropdown.list.rehab = xpath: (//XCUIElementTypeOther[@name="Rehab"])[2]
hagoC.filter.dropdown.list.rehab = xpath: (//XCUIElementTypeOther[@name="Rehab"])[2]

hagoE.filter.rehab.message = xpath: (//XCUIElementTypeOther[contains(@name, '您有一個康復提示定於下午05:37')])[3]
hagoC.filter.rehab.message = xpath: (//XCUIElementTypeOther[contains(@name, '您有一個康復提示定於下午05:37')])[3]

hagoE.filter.rehab.label = xpath: //XCUIElementTypeStaticText[@name="Rehab"]
hagoC.filter.rehab.label = xpath: //XCUIElementTypeStaticText[@name="Rehab"]

hagoE.filter.dropdown.list.payha = xpath: (//XCUIElementTypeOther[@name="Pay HA"])[2]
hagoC.filter.dropdown.list.payha = xpath: (//XCUIElementTypeOther[@name="Pay HA"])[2]

hagoE.filter.payha.message1 = xpath: (//XCUIElementTypeOther[contains(@name, 'Bill #BTKO2000031860U')])[3]
hagoC.filter.payha.message1 = xpath: (//XCUIElementTypeOther[contains(@name, 'Bill #BTKO2000031860U')])[3]

hagoE.filter.payha.message2 = xpath: (//XCUIElementTypeOther[contains(@name, '門診編號#SOPD1910837Z')])[3]
hagoC.filter.payha.message2 = xpath: (//XCUIElementTypeOther[contains(@name, '門診編號#SOPD1910837Z')])[3]

hagoE.filter.payha.label = xpath: //XCUIElementTypeStaticText[@name="Pay HA"]
hagoC.filter.payha.label = xpath: //XCUIElementTypeStaticText[@name="Pay HA"]

hagoE.filter.dropdown.list.ha = xpath: (//XCUIElementTypeOther[@name="Hospital Authority"])[2]
hagoC.filter.dropdown.list.ha = xpath: (//XCUIElementTypeOther[@name="Hospital Authority"])[2]

hagoE.filter.ha.label = xpath: //XCUIElementTypeStaticText[@name="Hospital Authority"]
hagoC.filter.ha.label = xpath: //XCUIElementTypeStaticText[@name="Hospital Authority"]

hagoE.filter.ha.message = xpath: (//XCUIElementTypeOther[contains(@name, 'HA General Message')])[3]
hagoC.filter.ha.message = xpath: (//XCUIElementTypeOther[contains(@name, 'HA General Message')])[3]

hagoE.undercare.user.icon = xpath: //XCUIElementTypeOther[@name="HA Go Logout "]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.undercare.user.icon = xpath: //XCUIElementTypeOther[@name="HA Go Logout "]/XCUIElementTypeOther[1]/XCUIElementTypeOther

hagoE.undercare.user.icons = xpath: //XCUIElementTypeOther[@name="Hello LAW, STRAWBERRY"][2]/XCUIElementTypeOther[1]
hagoC.undercare.user.icons = xpath: //XCUIElementTypeOther[@name="Hello LAW, STRAWBERRY"][2]/XCUIElementTypeOther[1]

hagoE.undercare.user = xpath: (//XCUIElementTypeOther[@name="LAU, SIU YUEN"])[2]
hagoC.undercare.user = xpath: (//XCUIElementTypeOther[@name="LAU, SIU YUEN"])[2]

hagoE.undercare.user.name = xpath: (//XCUIElementTypeOther[@name="LAU, SIU YUEN"])[4]
hagoC.undercare.user.name = xpath: (//XCUIElementTypeOther[@name="LAU, SIU YUEN"])[4]

hagoE.undercare.user.name.nc = xpath: //XCUIElementTypeOther[@name="LAU, SIU YUEN"]
hagoC.undercare.user.name.nc = xpath: //XCUIElementTypeOther[@name="LAU, SIU YUEN"]

hagoE.undercare.user.notification.center.icon = xpath: (//XCUIElementTypeOther[@name="LAU, SIU YUEN"])[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther
hagoC.undercare.user.notification.center.icon = xpath: (//XCUIElementTypeOther[@name="LAU, SIU YUEN"])[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther

################################################################
#BookHA: English version - Enquiry - begins
################################################################
mBookingE.search = aid: Enquiry
mBookingE.declaration = id: hk.org.ha.mbooking:id/cbDeclaration
mBookingE.confirm = id: hk.org.ha.mbooking:id/ibtnConfirm
mBookingE.continue = id: android:id/button1
mBookingE.applicationStatus = xpath: //XCUIElementTypeButton[@name="My Application Status"]
mBookingE.mb = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1] 
mBookingE.hkid1st = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[3]
mBookingE.hkid2nd = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[4]
mBookingE.hkid3rd = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[5]
mBookingE.done = aid: Done
mBookingE.next = aid: Next
mBookingE.confirmTestEnv = aid: Confirm
mBookingE.numPad1 = aid: 1
mBookingE.numPad2 = aid: 2
mBookingE.numPad3 = aid: 3
mBookingE.numPad4 = aid: 4
mBookingE.numPad5 = aid: 5
mBookingE.numPad6 = aid: 6
mBookingE.numPad7 = aid: 7
mBookingE.numPad8 = aid: 8
mBookingE.numPad9 = aid: 9
mBookingE.numPad0 = aid: 0
mBookingE.numPadA = aid: A
mBookingE.back = xpath: (//XCUIElementTypeButton[@name="Back"])[2]
mBookingE.myAppBack= coordinator: 20,45
mBookingE.enquiryBack = coordinator: 20,45

################################################################
#BookHA: English vresion - Submit Application - begins
################################################################
mBookingE.submitApplication = aid: Submit Application
mBookingE.saReceivedHealthcareYes = coordinator: 61,274
mBookingE.saReceivedHealthcareConfirm = coordinator: 77,458
mBookingE.saApplicationNoteContinue = coordinator: 80,515
mBookingE.saTestEnvConfirm = aid: Confirm
mBookingE.saID1stHKID = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
mBookingE.saID2ndHKID = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[2]
mBookingE.saID3rdHKID = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[3]
mBookingE.saIDSurname = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[4]
mBookingE.saIDGivenname = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[5]
mBookingE.saIDMobileNo = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[6]
mBookingE.saIDSpecialty= xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[7]
mBookingE.saIDSpecialtyValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[5]
mBookingE.saIDHospital = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[9]
mBookingE.saIDHospitalValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[6]
mBookingE.saIDDone = xpath: //XCUIElementTypeButton[@name="Done"]
mBookingE.saIDNext = aid: Next
mBookingE.saSDPhotoTaking = xpath: //XCUIElementTypeButton[@name="Photo taking"]
mBookingE.saGeneralPhotoCapture = xpath: //XCUIElementTypeButton[@name="PhotoCapture"] 158-528
mBookingE.saGeneralUsePhoto = xpath: //XCUIElementTypeStaticText[@name="使用相片"]
mBookingE.saSDNext = xpath: //XCUIElementTypeButton[@name="Next"]
mBookingE.srlOtherDoctorReferral = xpath: (//XCUIElementTypeButton[@name="Other doctor's referral letter"])[1]
mBookingE.srlPhotoTaking1 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]
mBookingE.srlPhotoTaking2 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]
mBookingE.srlPhotoTaking3 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
mBookingE.srlPhotoTaking4 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]
mBookingE.srlPhotoTakingButton = coordinator: 158,528
mBookingE.srlNext = xpath: //XCUIElementTypeButton[@name="Next"]
mBookingE.checkNext = xpath: //XCUIElementTypeButton[@name="Next"]
mBookingE.saGeneralFinish = coordinator: 152,542

################################################################
#HAGo: English version - Edit Profile - begins
################################################################
hagoE.login = xpath: //*[@name="Login"]
hagoE.search = aid: Enquiry
hagoE.username = xpath: (//XCUIElementTypeTextField)
hagoE.password = xpath: (//XCUIElementTypeSecureTextField)
hagoE.login2 = xpath: //XCUIElementTypeOther[@name="Login "] 
hagoE.useTouchIDNo = aid: Not now
hagoE.touchIDEnableLaterMsg = aid: OK
hagoE.logout = xpath: (//XCUIElementTypeOther[@name="Logout "])[2]
hagoE.logoutConfirmYes = aid: Yes
hagoE.profile = xpath: (//XCUIElementTypeOther[@name="Profile"])[2]
hagoE.emailEdit = xpath: (//XCUIElementTypeOther[@name="Edit"])[2]
hagoE.emailEditPassword = xpath: (//XCUIElementTypeOther[@name="Show"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField
hagoE.emailEditConfirm = xpath: (//XCUIElementTypeOther[@name="Confirm"])[2]
hagoE.emailEditNewEmail = xpath: //XCUIElementTypeOther[@name="Enter New Email Address"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField
hagoE.emailEditSave = xpath: (//XCUIElementTypeOther[@name="Save"])[2]
hagoE.emailEditConfirmLater = aid: Confirm later
hagoE.emailEditChangeNotEffective = aid: OK
hagoE.profileBackSymbol = xpath: (//XCUIElementTypeOther[@name="Profile"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoE.mobileEdit = xpath: (//XCUIElementTypeOther[@name="Edit"])[4]

################################################################
#HAGo: English version - My Appointment - begins
################################################################
hagoE.appMyappt = xpath: //XCUIElementTypeOther[@name="Appointments"]
hagoE.myapptTabFuture = xpath: //XCUIElementTypeOther[@name="FUTURETITLE"]
hagoE.myapptTabPast = xpath: //XCUIElementTypeOther[@name="PASTTITLE"]
hagoE.myapptBtnFutureShowCancelRescheduleRecord = xpath: (//XCUIElementTypeOther[@name="Show Cancelled & Rescheduled record"])[1]/XCUIElementTypeSwitch
hagoE.myapptBtnPastShowCancelRescheduleRecord = xpath: (//XCUIElementTypeOther[@name="Show Cancelled & Rescheduled record"])[2]/XCUIElementTypeSwitch
hagoE.myapptBtnMyapptBackSymbol = xpath: (//XCUIElementTypeOther[@name="My Appointments"])[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther

################################################################
#BookHA: Chinese version - Enquiry - begins
################################################################
mBookingC.search = aid: 我的查詢
mBookingC.declaration = id: hk.org.ha.mbooking:id/cbDeclaration
mBookingC.confirm = id: hk.org.ha.mbooking:id/ibtnConfirm
mBookingC.continue = id: android:id/button1
mBookingC.applicationStatus = xpath: //XCUIElementTypeButton[@name="我的預約狀況"]
mBookingC.mb = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1] 
mBookingC.hkid1st = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[3]
mBookingC.hkid2nd = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[4]
mBookingC.hkid3rd = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[5]
mBookingC.done = aid: Done
mBookingC.next = aid: 下一步
mBookingC.confirmTestEnv = aid: 確定
mBookingC.numPad1 = aid: 1
mBookingC.numPad2 = aid: 2
mBookingC.numPad3 = aid: 3
mBookingC.numPad4 = aid: 4
mBookingC.numPad5 = aid: 5
mBookingC.numPad6 = aid: 6
mBookingC.numPad7 = aid: 7
mBookingC.numPad8 = aid: 8
mBookingC.numPad9 = aid: 9
mBookingC.numPad0 = aid: 0
mBookingC.numPadA = aid: A
mBookingC.back = (//XCUIElementTypeButton[@name="返回"])[2]
mBookingC.myAppBack= coordinator: 20,45
mBookingC.enquiryBack = coordinator: 20,45

################################################################
#BookHA: Chinese version - Submit Application - begins
################################################################
mBookingC.submitApplication = aid: 遞交預約
mBookingC.saReceivedHealthcareYes = coordinator: 61,224
mBookingC.saReceivedHealthcareConfirm = coordinator: 77,458
mBookingC.saApplicationNoteContinue = coordinator: 80,515
mBookingC.saTestEnvConfirm = aid: 確定
mBookingC.saID1stHKID = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]
mBookingC.saID2ndHKID = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[2]
mBookingC.saID3rdHKID = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[3]
mBookingC.saIDSurname = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[4]
mBookingC.saIDGivenname = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[5]
mBookingC.saIDMobileNo = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[6]
mBookingC.saIDSpecialty= xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[7]
mBookingC.saIDSpecialtyValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[5]
mBookingC.saIDHospital = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther[1]/XCUIElementTypeTextField[9]
mBookingC.saIDHospitalValue = xpath: /hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[6]
mBookingC.saIDDone = xpath: //XCUIElementTypeButton[@name="完成"]
mBookingC.saIDNext = aid: 下一步
mBookingC.saSDPhotoTaking = xpath: (//XCUIElementTypeButton[@name="拍攝影像"])[2]
mBookingC.saGeneralPhotoCapture = xpath: //XCUIElementTypeButton[@name="PhotoCapture"]
mBookingC.saGeneralUsePhoto = xpath: //XCUIElementTypeStaticText[@name="使用相片"]
mBookingC.saSDNext = aid: 下一步
mBookingC.srlOtherDoctorReferral = xpath: (//XCUIElementTypeButton[@name="其他醫生轉介信"])[2]
mBookingC.srlPhotoTaking1 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]
mBookingC.srlPhotoTaking2 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]
mBookingC.srlPhotoTaking3 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]
mBookingC.srlPhotoTaking4 = xpath: //XCUIElementTypeApplication[@name="HA Go (FPS)"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[4]
mBookingC.srlPhotoTakingButton = coordinator: 158,528
mBookingC.srlNext = aid: 下一步
mBookingC.checkNext = aid: 下一步
mBookingC.saGeneralFinish = coordinator: 152,542

################################################################
#HAGo: Chinese version - Edit Profile - begins
################################################################
hagoC.login = xpath: //*[@name="登入"]
hagoC.search = aid: Enquiry
hagoC.username = xpath: //XCUIElementTypeOther[@name="用戶名稱 忘記用戶名稱 密碼 忘記密碼 顯示 登入 "]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTextField
hagoC.password = xpath: (//XCUIElementTypeOther[@name="顯示"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField
hagoC.login2 = xpath: //XCUIElementTypeOther[@name="登入 "] 
hagoC.useTouchIDNo = aid: 稍後
hagoC.touchIDEnableLaterMsg = aid: 確定
hagoC.logout = xpath: (//XCUIElementTypeOther[@name="登出 "])[2]
hagoC.logoutConfirmYes = aid: 是
hagoC.profile = xpath: (//XCUIElementTypeOther[@name="個人資料"])[3]
hagoC.emailEdit = xpath: (//XCUIElementTypeOther[@name="更改"])[2]
hagoC.emailEditPassword = xpath: (//XCUIElementTypeOther[@name="顯示"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeSecureTextField
hagoC.emailEditConfirm = xpath: (//XCUIElementTypeOther[@name="確認"])[2]
hagoC.emailEditNewEmail = xpath: //XCUIElementTypeOther[@name="請輸入新電郵地址"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField
hagoC.emailEditSave = xpath: (//XCUIElementTypeOther[@name="儲存"])[2]
hagoC.emailEditConfirmLater = aid: 稍後確認
hagoC.emailEditChangeNotEffective = aid: 確定
hagoC.profileBackSymbol = xpath: (//XCUIElementTypeOther[@name="個人資料"])[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther
hagoC.mobileEdit = xpath: (//XCUIElementTypeOther[@name="更改"])[4]

################################################################
#HAGo: Chinese version - My Appointment - begins
################################################################
hagoC.appMyappt = xpath: (//XCUIElementTypeOther[@name="預約紀錄"])[2]
hagoC.myapptTabFuture = xpath: //XCUIElementTypeOther[@name="FUTURETITLE"]
hagoC.myapptTabPast = xpath: //XCUIElementTypeOther[@name="PASTTITLE"]
hagoC.myapptBtnFutureShowCancelRescheduleRecord = xpath: (//XCUIElementTypeOther[@name="顯示改期及取消紀錄"])[1]/XCUIElementTypeSwitch
hagoC.myapptBtnPastShowCancelRescheduleRecord = xpath: (//XCUIElementTypeOther[@name="顯示改期及取消紀錄"])[2]/XCUIElementTypeSwitch
hagoC.myapptBtnMyapptBackSymbol = xpath: (//XCUIElementTypeOther[@name="預約紀錄"])[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther




################################################################
#HAGo: Pay HA
################################################################
hagoE.payha.icon = 
hagoE.payha.view = aid: ViewBtn
hagoE.payha.total = 
