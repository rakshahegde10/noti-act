# Setup environment for mobile automation

A. Download and install the latest version nodejs

> https://nodejs.org/en/download/

Make sure npm works after installation by typing below command in cmd/terminal

    npm

B. Install latest version of appium by using below command in cmd/terminal

    npm install -g appium
    
Check if the installation is successful by running below command

    appium -a 0.0.0.0 -p 4728
    
You should see the first line as Welcome to Appium v${versioNumber}
 
C. Install GUI version of appium using below link

    https://github.com/appium/appium-desktop/releases/tag/v1.17.1-1

D. Install Android Studio

    https://developer.android.com/studio
    
E. Set ANDROID_HOME path

    /Users/{YOUR_USER_NAME}/Library/Android/sdk
    
The path will be different for Window machine. You can go to Android Studio -> File -> Project Structure -> SDKs to find your installed SDK path

# Setup IDE

1. Download Eclipse
2. Create the repository folder at anywhere and copy the path
3. Open settings(HA).xml and scroll to the bottom to change the local repo path to the new repository folder path
4. Go to Eclipse -> Preferences -> Maven -> User Settings
5. Change the User Settings path to the path of settings(HA).xml on your machine
6. Click update, apply settings, then apply and close
7. Go to Help -> Eclipse Marketplace
8. Search for Cucumber in query text field
9. Install Cucumber Eclipse Plugin
10. Restart Eclipse
11. Right click taf-dsl-ha-go-lib in Project Explorer
12. Click Maven -> Update Project
13. Right click taf-dsl-ha-go-lib in Project Explorer
14. Run as -> Maven Install
15. Confirm installation successful (If failed, please restart the IDE and try again)

# Start Appium Server Hub Node on Mac

1. Open terminal and navigate to the lib folder in selenium folder

> ‎⁨Macintosh HD⁩ ▸ ⁨Users⁩ ▸ ⁨twl640⁩ ▸ ⁨Desktop⁩ ▸ ⁨ACT⁩ ▸ ⁨asl⁩ ▸ ⁨asl⁩ ▸ ⁨selenium⁩ ▸ ⁨act-selenium-server⁩ ▸ ⁨SeleniumServer_v3_12_0⁩ ▸ ⁨lib⁩

2. Start Hub on Mac

> Java -cp taf-selenium-matcher-lib-1.0.1.jar:selenium-server-standalone-3.12.0.jar:selenium-video-node-2.7.jar org.openqa.grid.selenium.GridLauncherV3 -servlets com.aimmac23.hub.servlet.HubVideoDownloadServlet -role hub -port 8094 -hubConfig hubConfig.json
pause

3. Create Node config.json for the target device and save it anywhere

4. Start Node on Mac

> appium -a 0.0.0.0 -p 8485 --nodeconfig 'nodeconfig path'

```
appium -a 0.0.0.0 -p 8485 --nodeconfig ~/Desktop/ACT/asl/asl/selenium/android_emulator.json
```

# Run Test

Below is an example of run command. Please change the values of the parameters accordingly on your machine.

> test -Dappium.remote.address=http://localhost:8094/wd/hub -DdataFilePath=src/test/resources/datafile/dev/ha_go_dataset.xlsx -Dmobile.properties.profile=Android_hago_emulator -Dtest.env=dev -DexecutionTag=@test -DfeatureFilePath=src/test/resources/features/notification_go.feature -Dvideo.capture=false -Dmobile.capability.app=/Users/twl640/Desktop/ACT/asl/hago.apk

# Optional setup

## Android
A. Open Android Virtual Device Manager in Android Studio if you need to run on Android emulator (Optional)

Install an Android virtual device of the latest version.

## iOS

> You can ignore the below steps if you are able to run appium on the real iOS device without setting anything (xcode and appium should be able to set up for iOS automatically upon first launch, but not always successful)

A. Install latest version xcode if you need to run on real iOS device

B. Prepare a valid apple developer account

C. Navigate to the below path and follow the steps

    /Applications/Appium.app/Contents/Resources/app/node_modules/appium/node_modules/appium-webdriveragent
    
    1. Run the WebDriverAgent.xcodeproj file at the above path in xcode
    2. Select WebDriverAgentRunner as scheme
    3. Select your target iOS device connected to the macOs as target
    4. Select WebDriverAgentLib under Targets
    5. Under Signing & Capabilities tab, choose Team of the apple developer account\
    6. Select WebDriverAgentRunner under Targets
    7. Under Signing & Capabilities tab, choose Team of the apple developer account\
    8. Click run to build
    9. Select WebDriverAgentLib as scheme
    10. Click run to build