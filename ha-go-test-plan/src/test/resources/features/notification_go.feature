Feature: HAGo-NC

@MobileTest @2.3.1 @Phase1 @GO-1 @ok
    Scenario Outline: Guest Mode - General and Rehab Notifications Retrieval and launch General Message details @Device:Mobile
        Given I am on HA Go Mobile landing page in "<app_language>" version
        And I have switched font size to "<font_size>"
        And I navigate to login page
        And I input "HAGON-1.username" into "Login username" text field
        And I input "HAGON-1.password" into "Login password" text field
        And I click "Login" button
        And I fill in verification code
        When I navigate to Notification Center after login with "HAGON-1.username"
        And I go back to home page from "Notification Center" and wait for "30" seconds
        And I logout and return to landing page
        And I navigate to Notification Center
        Then I verify "HAGON-1.rehabNotificationContent" is above "General Messages"
        And I click "first Hospital Authority" notification
        And I verify "first Hospital Authority notification" is shown correctly

        Examples:
            | app_language | font_size  |
            | English      | Small     |
            #| Chinese      | Medium     |

@MobileTest @2.3.2 @Phase1 @GO-2 @ok
		Scenario Outline: Login Mode – General and TeleInfo Notifications Retrieval and launch TeleInfo link @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-2.username" into "Login username" text field
		    And I input "HAGON-2.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    When I navigate to Notification Center after login with "HAGON-2.username"
		    And I go back to home page from "Notification Center" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-2.username"
		    And I click "HAGON-2.username" "HAGON-2.miniAppName"
		    Then I verify "HAGON-2.telecareNotificationContent" is shown correctly
		    And I click "詳情" link
		
		    Examples:
		        | app_language | font_size  |
		        | English      | Large     |
		        #| Chinese      | Medium     |
        
@MobileTest @2.3.3 @Phase1 @GO-3 @ok
		Scenario Outline: Login Mode – General, Rehab, PayHA, Appointment Retrieval and launch all Mini-App  @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-1.username" into "Login username" text field
		    And I input "HAGON-1.password" into "Login password" text field
		    And I click "Login" button
		    When I navigate to Notification Center after login with "HAGON-1.username"
		    And I go back to home page from "Notification Center" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "HAGON-1.username" "HAGON-1.miniAppName"
		    Then I verify "HAGON-1.miniAppName" is shown correctly
		    And I go back to home page from "My Appointments" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "HAGON-1.username" "HAGON-1.miniAppName1"
		    Then I verify "HAGON-1.miniAppName1" is shown correctly
		    And I go back to home page from "Rehab" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "HAGON-1.username" "HAGON-1.miniAppName3"
		    Then I verify "HAGON-1.miniAppName3" is shown correctly
		
		    Examples:
		        | app_language | font_size  |
		        | English      | Medium     |
		        #| Chinese      | Medium     |
       
        
@MobileTest @2.3.4 @Phase2 @GO-4 @ok @retest
	  Scenario Outline: Login Mode - Notifications Count @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-2.username" into "Login username" text field
		    And I input "HAGON-2.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    And I navigate to Notification Center after login with "HAGON-2.username"
		    And I go back to home page from "Notification Center" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-2.username"
		    Then I verify Un-read Notification Count is "1"
		    Then I verify "Red Badge" next to first notification
		    And I click "HAGON-2.username" "HAGON-2.miniAppName"
		    And I go back to notification center
		    Then I verify Un-read Notification Count is "0"
		    Then I verify "Red Badge disappears" next to first notification
		    
		    Examples:
		        | app_language | font_size |
		        | English      | Small     |
		        #| Chinese      | Small     |
        
@MobileTest @2.3.5 @Phase2 @GO-5 @ok @retest
    Scenario Outline: Login Mode - Search Notifications and highlight search text @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-2.username" into "Login username" text field
		    And I input "HAGON-2.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    And I navigate to Notification Center after login with "HAGON-2.username"
		    And I go back to home page from "Notification Center" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-2.username"
		    Then I click on Search Icon and the SearchBar is displayed
        Then I input Search string "outpatient" into SearchBar text field
        Then I verify "second Hospital Authority" is shown in Search Results
        Then I verify "second Hospital Authority" notification is "Expanded"
        And I verify "outpatient" is highlighted
        Then I clear the Search text
        Then I verify "second Hospital Authority" notification is "Collapsed"
		        
		    Examples:
		        | app_language | font_size |
		        | English      | Large   |
		        #| Chinese      | Small    |
		        
@MobileTest @2.3.6 @Phase2 @GO-6 @ok @retest
    Scenario Outline: Login Mode - Filter Option @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-1.username" into "Login username" text field
		    And I input "HAGON-1.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I go back to home page from "Notification Center" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    Then I click on Search Icon and the SearchBar is displayed
        Then I click inside the SearchBar and a dropdown of Filter Options is displayed
        Then I click Filter Option "Rehab" 
        Then I verify "Rehab" notifications is shown in Search Results
        Then I click Filter Option "PayHA" 
        Then I verify "PayHA" notifications is shown in Search Results
        Then I click Filter Option "Hospital Authority" 
        Then I verify "Hospital Authority" notifications is shown in Search Results
        Then I input Search string "outpatient" into SearchBar text field
        Then I verify "second Hospital Authority" is shown in Search Results
        And I verify "outpatient" is highlighted
		        
		    Examples:
		        | app_language | font_size |
		        | English      | Medium   |
		        #| Chinese      | Small    |
       
@MobileTest @2.3.7 @Phase2 @GO-7 @ok @retest
    Scenario Outline: Login Mode - Undercare User notification retrieval and Un-read count Update @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-3.username" into "Login username" text field
		    And I input "HAGON-3.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    And I navigate to Undercare User Icon
		    Then I click on caree user "LAU, SIU YUEN"
        Then I verify user "LAU, SIU YUEN" is displayed
        And I navigate to caree Notification Center 
        Then I verify user "LAU, SIU YUEN" is displayed
        And I verify "HAGON-3.myAppointmentNotificationContent" is above "General Messages"
        And I verify "HAGON-3.rehabNotificationContent" is above "HAGON-3.myAppointmentNotificationContent"
        Then I verify Un-read Notification Count is "5"
        And I click "HAGON-3.username" "HAGON-3.miniAppName"
		    Then I verify "HAGON-3.miniAppName" is shown correctly
		    And I go back to home page from "Rehab" and wait for "30" seconds
		    And I navigate to caree Notification Center 
		    Then I verify Un-read Notification Count is "5"
            
		    Examples:
		        | app_language | font_size |
		        | English      | Small   |
		        #| Chinese      | Small    |
		        
@MobileTest @2.3.8 @Phase2 @GO-8 @ok @retest
    Scenario Outline: Guest Mode - Notification Bell Indicator (General Messages) @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I have switched font size to "<font_size>"
		    And I navigate to login page
		    And I input "HAGON-1.username" into "Login username" text field
		    And I input "HAGON-1.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I go back to home page from "Notification Center" and wait for "30" seconds
		    And I logout and return to landing page
		    Then I verify notification center bell icon has "Message" indicator
		    And I navigate to Notification Center
		    Then I verify Hospital Authority messages are in correct order
		    And I click "first Hospital Authority" notification
		    And I go back to notification center
		    And I click "second Hospital Authority" notification
		    And I go back to notification center
		    And I click "third Hospital Authority" notification
		    And I go back to notification center
		    And I go back to home page from "Notification Center" and wait for "30" seconds
        Then I verify notification center bell icon has "No" indicator
            
		    Examples:
		        | app_language | font_size |
		        | English      | Small   |
		        #| Chinese      | Small    |

@MobileTest @2.3.7 @Phase2 @GO-9 @ok @retest
    Scenario Outline: Login Mode - Notification Bell Indicator (General and Personal Messages) @Device:Mobile
		    Given I am on HA Go Mobile landing page in "<app_language>" version
		    And I navigate to login page
		    And I input "HAGON-1.username" into "Login username" text field
		    And I input "HAGON-1.password" into "Login password" text field
		    And I click "Login" button
		    And I fill in verification code
		    Then I verify notification center bell icon has "Message" indicator
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "HAGON-1.username" "HAGON-1.miniAppName3"
		    And I go back to home page from "PayHA" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "HAGON-1.username" "HAGON-1.miniAppName1"
		    And I go back to home page from "Rehab" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
	      And I click "HAGON-1.username" "HAGON-1.miniAppName2"
		    And I go back to home page from "PayHA" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "HAGON-1.username" "HAGON-1.miniAppName"
		    And I go back to home page from "My Appointments" and wait for "30" seconds
		    And I navigate to Notification Center after login with "HAGON-1.username"
		    And I click "first Hospital Authority" notification
		    And I go back to notification center
		    And I click "second Hospital Authority" notification
		    And I go back to notification center
		    And I click "third Hospital Authority" notification
		    And I go back to notification center
		    And I go back to home page from "Notification Center" and wait for "30" seconds
        Then I verify notification center bell icon has "No" indicator
            
		    Examples:
		        | app_language | font_size |
		        | English      | Small   |
		        #| Chinese      | Small    |
