# Feature file syntax
Please refer to the following link if you are unfamiliar with Gherkin syntax.

> https://cucumber.io/docs/gherkin/

A simple example of a valid scenario is like below.
    
    Scenario: Some test case --> Scenario name
        Given I have navigated to a page --> The first step
        When I do something --> The second step
        And I click a button --> The third step
        Then I should see something --> The last step

> P.S: It's not necessary to have all three keywords, Given, When, Then in a scenario for it to be valid. For complicated scenarios with similiar steps, please consider using Scenario Outline with Examples like shown below.


    Scenario Outline: Some test case with examples
        Given I have navigated to "<some_page>"
        When I do something
        And I click a button
        Then I should see something
     
     Examples:
        | some_page |
        | a page    |
        | b page    |
        
# Fetching values from excel

Currently, the function

    getValueFromExcel(String filter)

is used to get values from excel.

    Example excel:
    
    | filter | a_column | b_column |
    | AA-1   | a        | b        |
    | AA-2   | c        | d        |
    | AA-3   | e        | f        |
    
In the above example, if you pass AA-1.a_column into the getValueFromExcel function, then it will return "a" as a string.
If you pass AA-2.b_column to the function, it will return "d" as a string, and so on and so forth.

> PS: The filter of AA- has to be added to the function as a filter constant value for the function to work. Therefore, if you are to change the filter value from existing values, AAIC-, SAAM-, DxPx-, to something else, please modify the function as well.

# Getting xPath
Please refer to the following link if you are unfamiliar with xPath syntax.

> https://www.guru99.com/xpath-selenium.html

You are advised to use Google Chrome to find xPath since it's one of the most stable browsers available on the market. However, there might be some differences regarding to the structure of the html page rendered on different browsers, so you might find that the xPath you find on Chrome does not work on IE or Edge, which are the two most unstable browsers. Should you face this problem, please confirm the xPath you are using on IE or Edge and modify the xPath specifically for IE and Edge using isIE() function to handle the problem.

 Adding some extension tool on Chrome like ChromePath can make your life easier while locating element.

 Please note that the flexibility and extendability xPath provides can never be replaced by using pure Id.

 You might create a xPath using wildcard and element id like shown below.

    //*[@id='some id']

For example, if you want to confirm a button named Button exists after some id, you might write something like this.

    //*[@id='some id']//following::button[text()='Button']
    
# Java matching steps

    Given I navigate to "some" page
    And I do something
    When I click something
    And I make something
    
The below function will match the step above.

    	@When("^I navigate to \"([^\"]*)\" page$")
    	public void iNavgitateToSomePage(String pageName) throws Exception {
    	    //...selenium codes...
    	} 

The function name of the step doesn't affect anything.

The statement of steps must be unique.
    